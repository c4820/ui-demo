import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {forkJoin, mergeMap, Observable, of, take} from 'rxjs';
import {Inject, Injectable} from '@angular/core';
import {PROPERTY_SERVICE} from '../constants/injectors';
import {IPropertyService} from '../services/interfaces/apis/property-service.interface';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ApplicationComponentChosenChannelEvent
} from '../models/channel-events/components/application-component-chosen.channel-event';
import {ComponentPropertyPageQueryParams} from '../models/query-params/component-property-page-query-params';
import {Application} from '../models/entities/application';
import {ApplicationVersion} from '../models/entities/application-version';
import {Component} from '../models/entities/component';
import {map} from 'rxjs/operators';
import {ComponentPropertyChosenChannelEvent} from '../models/channel-events/component-property-chosen.channel-event';
import {Property} from '../models/entities/property';

@Injectable()
export class ComponentPropertyPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ComponentPropertyPageQueryParams;

    if (!queryParams.hostId || !queryParams.hostKind) {
      return false;
    }
    return this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .pipe(
        take(1),
        mergeMap(({application, version, component}) => {
          let getPropertyObservable = of(new Property(''));
          if (queryParams.propertyId) {
            getPropertyObservable = this._propertyService.getByIdAsync(queryParams.propertyId!);
          }
          return forkJoin([
            of(application), of(version), of(component), getPropertyObservable
          ]);
        }),
        map(([application, version, component, property]) => {
          this._messageBusService
            .addMessageInstance(new ComponentPropertyChosenChannelEvent(
              application, version, component, property, queryParams.hostId, queryParams.hostKind
            ));
          return true;
        })
      );

  }

  //#endregion
}
