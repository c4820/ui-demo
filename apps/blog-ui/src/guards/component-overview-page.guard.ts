import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {forkJoin, mergeMap, Observable, of, take} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {COMPONENT_SERVICE, PROPERTY_SERVICE} from '../constants/injectors';
import {IComponentService} from '../services/interfaces/apis/component-service.interface';
import {IPropertyService} from '../services/interfaces/apis/property-service.interface';
import {ApplicationComponentPageQueryParams} from '../models/query-params/application-component-page-query-params';
import {
  ApplicationVersionSelectedChannelEvent
} from '../models/channel-events/applications/application-version-selected.channel-event';
import {map} from 'rxjs/operators';
import {
  ApplicationComponentChosenChannelEvent
} from '../models/channel-events/components/application-component-chosen.channel-event';
import {Component} from '../models/entities/component';

@Injectable()
export class ComponentOverviewPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ApplicationComponentPageQueryParams;

    return this._messageBusService
      .hookMessageChannelByType(ApplicationVersionSelectedChannelEvent)
      .pipe(
        take(1),
        mergeMap(({application, version}) => {

          let getComponentByIdObservable = of(new Component(''));
          if (queryParams.componentId) {
            getComponentByIdObservable = this._componentService.getByIdAsync(queryParams.componentId);
          }

          return forkJoin([of(application), of(version), getComponentByIdObservable]);
        }),
        map(([application, version, component]) => {
          this._messageBusService.addMessageInstance(new ApplicationComponentChosenChannelEvent(
            application,
            version!,
            component
          ));
          return true;
        })
      );
  }

  //#endregion

}
