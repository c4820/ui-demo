import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {forkJoin, mergeMap, Observable, of, take, tap} from 'rxjs';
import {
  APPLICATION_SERVICE,
  APPLICATION_VERSION_SERVICE,
  COMPONENT_SERVICE,
  EVENT_SERVICE,
  METHOD_SERVICE,
  PROPERTY_SERVICE
} from '../constants/injectors';
import {IComponentService} from '../services/interfaces/apis/component-service.interface';
import {ApplicationComponentsPageQueryParams} from '../models/query-params/application-components-page-query-params';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {map} from 'rxjs/operators';
import {IPropertyService} from '../services/interfaces/apis/property-service.interface';
import {IMethodService} from '../services/interfaces/apis/method-service.interface';
import {IEventService} from '../services/interfaces/apis/event-service.interface';
import {
  ApplicationSelectedChannelEvent
} from '../models/channel-events/applications/application-selected.channel-event';
import {IApplicationService} from '../services/interfaces/apis/application-service.interface';
import {
  ApplicationVersionSelectedChannelEvent
} from '../models/channel-events/applications/application-version-selected.channel-event';
import {IApplicationVersionService} from '../services/interfaces/apis/application-version-service.interface';
import {ApplicationVersion} from '../models/entities/application-version';

@Injectable()
export class ApplicationComponentsPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService,
                     @Inject(METHOD_SERVICE)
                     protected readonly _methodService: IMethodService,
                     @Inject(EVENT_SERVICE)
                     protected readonly _eventService: IEventService,
                     @Inject(APPLICATION_SERVICE)
                     protected readonly _applicationService: IApplicationService,
                     @Inject(APPLICATION_VERSION_SERVICE)
                     protected readonly _applicationVersionService: IApplicationVersionService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ApplicationComponentsPageQueryParams;
    if (!queryParams.applicationId) {
      return of(false);
    }

    return this._messageBusService
      .hookMessageChannelByType(ApplicationSelectedChannelEvent)
      .pipe(
        take(1),
        mergeMap(({application}) => {
          let getVersionObservable = of(new ApplicationVersion('', '', '', []));
          if (queryParams.versionId && queryParams.versionId.length) {
            getVersionObservable = this._applicationVersionService.getByIdAsync(queryParams.versionId);
          }

          return forkJoin([of(application), getVersionObservable]);
        }),
        tap(([application, version]) => {
          this._messageBusService.addMessageInstance(new ApplicationVersionSelectedChannelEvent(
            application,
            version
          ));
        }),
        map(() => true)
      );
  }

  //#endregion

}
