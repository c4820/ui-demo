import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {forkJoin, mergeMap, Observable, of, take} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {COMPONENT_SERVICE, METHOD_SERVICE, PROPERTY_SERVICE} from '../constants/injectors';
import {IComponentService} from '../services/interfaces/apis/component-service.interface';
import {Application} from '../models/entities/application';
import {ApplicationVersion} from '../models/entities/application-version';
import {map} from 'rxjs/operators';
import {
  ApplicationComponentChosenChannelEvent
} from '../models/channel-events/components/application-component-chosen.channel-event';
import {Component} from '../models/entities/component';
import {IMethodService} from '../services/interfaces/apis/method-service.interface';
import {Method} from '../models/entities/method';
import {
  ComponentMethodChosenChannelEvent
} from '../models/channel-events/components/component-method-chosen.channel-event';
import {ComponentMethodPageQueryParams} from '../models/query-params/component-method-page-query-params';

@Injectable()
export class ComponentMethodPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(METHOD_SERVICE)
                     protected readonly _methodService: IMethodService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ComponentMethodPageQueryParams;

    return this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .pipe(
        take(1),
        mergeMap(({application, version, component}) => {

          let getMethodByIdObservable = of(new Method(''));
          if (queryParams.methodId) {
            getMethodByIdObservable = this._methodService.getByIdAsync(queryParams.methodId);
          }

          return forkJoin([of(application), of(version), of(component), getMethodByIdObservable]);
        }),
        map(([application, version, component, method]) => {
          this._messageBusService.addMessageInstance(new ComponentMethodChosenChannelEvent(
            application,
            version,
            component,
            method
          ));
          return true;
        })
      );
  }

  //#endregion

}
