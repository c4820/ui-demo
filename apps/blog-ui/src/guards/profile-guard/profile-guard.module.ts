import {NgModule} from '@angular/core';
import {ProfileGuard} from './profile.guard';

@NgModule({
  providers: [
    ProfileGuard
  ]
})
export class ProfileGuardModule {
}
