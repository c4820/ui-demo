import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LocalStorageService} from 'ngx-webstorage';
import {StorageKeys} from '../../constants/storage-keys';
import {IAuthenticationResult} from '../../interfaces/idp/authentication-result.interface';
import jwtDecode from 'jwt-decode';
import {ProfileViewModel} from '../../view-models/profiles/profile.view-model';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {ProfileUpdatedChannelEvent} from '../../models/channel-events/profiles/profile-updated.channel-event';

@Injectable()
export class ProfileGuard implements CanActivate {

  //#region Constructor

  public constructor(
    @Inject(MESSAGE_BUS_SERVICE)
    protected readonly _messageBusService: IMessageBusService,
    protected readonly _storage: LocalStorageService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    try {
      const authenticationResult = this._storage
        .retrieve(StorageKeys.authenticationResult) as IAuthenticationResult;

      if (!authenticationResult) {
        throw new Error('INVALID_AUTHENTICATION_RESULT');
      }

      const accessToken = authenticationResult.accessToken;
      if (!accessToken) {
        throw new Error('INVALID_ACCESS_TOKEN');
      }

      const decodedToken = jwtDecode<Record<string, string>>(accessToken);
      const issuedAt = new Date(parseInt(decodedToken['iat'], 10) * 1000);
      const expiryTime = new Date(parseInt(decodedToken['exp'], 10) * 1000);
      const profile = new ProfileViewModel(decodedToken['sub'], issuedAt, expiryTime);
      profile.picture = decodedToken['picture'];
      profile.email = decodedToken['email'];
      profile.verified = Boolean(decodedToken['email_verified']).valueOf();

      this._messageBusService.addMessageInstance(new ProfileUpdatedChannelEvent(profile));
      return true;
    }
    catch (exception) {
      this._messageBusService.addMessageInstance(new ProfileUpdatedChannelEvent(undefined));
      return true;
    }
  }

  //#endregion

}
