import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {forkJoin, mergeMap, Observable, of, take} from 'rxjs';
import {APPLICATION_SERVICE, APPLICATION_VERSION_SERVICE, COMPONENT_SERVICE} from '../constants/injectors';
import {IComponentService} from '../services/interfaces/apis/component-service.interface';
import {ApplicationPageQueryParams} from '../models/query-params/application-page-query-params';
import {IApplicationService} from '../services/interfaces/apis/application-service.interface';
import {catchError, map} from 'rxjs/operators';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ApplicationVersionSelectedChannelEvent
} from '../models/channel-events/applications/application-version-selected.channel-event';
import {ApplicationVersion} from '../models/entities/application-version';
import {
  ApplicationSelectedChannelEvent
} from '../models/channel-events/applications/application-selected.channel-event';
import {Application} from '../models/entities/application';
import {IApplicationVersionService} from '../services/interfaces/apis/application-version-service.interface';

@Injectable()
export class ApplicationVersionPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(APPLICATION_SERVICE)
                     protected readonly _applicationService: IApplicationService,
                     @Inject(APPLICATION_VERSION_SERVICE)
                     protected readonly _applicationVersionService: IApplicationVersionService,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ApplicationPageQueryParams;
    if (!queryParams.applicationId) {
      return false;
    }

    return this._messageBusService
      .hookMessageChannelByType(ApplicationSelectedChannelEvent)
      .pipe(
        take(1),
        mergeMap(({application}) => {

          let getApplicationVersionObservable = of(new ApplicationVersion('', '', ''));
          if (queryParams.versionId) {
            getApplicationVersionObservable = this._applicationVersionService.getByIdAsync(queryParams.versionId);
          }

          return forkJoin([of(application), getApplicationVersionObservable]);
        }),
        map(([application, version]) => {
          this._messageBusService.addMessageInstance(new ApplicationVersionSelectedChannelEvent(application!, version));
          return true;
        }),
        catchError(() => of(false))
      );
  }

  //#endregion
}
