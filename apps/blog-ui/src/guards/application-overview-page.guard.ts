import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {ApplicationOverviewQueryParams} from '../models/query-params/application-overview-query-params';
import {ISmartNavigatorService, SMART_NAVIGATOR_SERVICE} from '@ui-tool/core';
import {ScreenCodes} from '../constants/screen-codes';
import {APPLICATION_SERVICE} from '../constants/injectors';
import {IApplicationService} from '../services/interfaces/apis/application-service.interface';
import {catchError, map} from 'rxjs/operators';
import {
  ApplicationVersionSelectedChannelEvent
} from '../models/channel-events/applications/application-version-selected.channel-event';
import {Application} from '../models/entities/application';

@Injectable()
export class ApplicationOverviewPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     @Inject(APPLICATION_SERVICE)
                     protected readonly _applicationService: IApplicationService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const queryParams = activatedRouteSnapshot.queryParams as ApplicationOverviewQueryParams;

    if (!queryParams.applicationId) {
      this._messageBusService.addMessageInstance(new ApplicationVersionSelectedChannelEvent(new Application('')));
      return of(true);
    }

    // Get the application
    return this._applicationService.getByIdAsync(queryParams.applicationId)
      .pipe(
        map(application => {
          this._messageBusService.addMessageInstance(new ApplicationVersionSelectedChannelEvent(application));
          return true;
        }),
        catchError(() => of(false))
      );
  }

  //#endregion

}
