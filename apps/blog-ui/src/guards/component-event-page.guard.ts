import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {forkJoin, mergeMap, Observable, of, take} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ApplicationComponentChosenChannelEvent
} from '../models/channel-events/components/application-component-chosen.channel-event';
import {Application} from '../models/entities/application';
import {ApplicationVersion} from '../models/entities/application-version';
import {Component} from '../models/entities/component';
import {ComponentEventPageQueryParams} from '../models/query-params/component-event-page-query-params';
import {COMPONENT_SERVICE, EVENT_SERVICE} from '../constants/injectors';
import {IComponentService} from '../services/interfaces/apis/component-service.interface';
import {IEventService} from '../services/interfaces/apis/event-service.interface';
import {catchError, map} from 'rxjs/operators';
import {
  ApplicationComponentEventChosenChannelEvent
} from '../models/channel-events/components/application-component-event-chosen.channel-event';
import {Event} from '../models/entities/event';

@Injectable()
export class ComponentEventPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(EVENT_SERVICE)
                     protected readonly _eventService: IEventService) {
  }

//#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ComponentEventPageQueryParams;
    return this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .pipe(
        take(1),
        mergeMap(({application, version, component}) => {

          let getEventObservable = of(new Event(''));
          if (queryParams.eventId) {
            getEventObservable = this._eventService.getByIdAsync(queryParams!.eventId!);
          }
          return forkJoin([of(application), of(version), of(component), getEventObservable]);
        }),
        map(([application, version, component, event]) => {
          this._messageBusService.addMessageInstance(new ApplicationComponentEventChosenChannelEvent(
            application, version, component, event
          ));
          return true;
        })
      );
  }

  //#endregion
}
