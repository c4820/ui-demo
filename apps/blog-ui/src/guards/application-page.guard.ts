import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Inject, Injectable} from '@angular/core';
import {Observable, of, tap} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {APPLICATION_SERVICE} from '../constants/injectors';
import {IApplicationService} from '../services/interfaces/apis/application-service.interface';
import {ApplicationPageQueryParams} from '../models/query-params/application-page-query-params';
import {
  ApplicationSelectedChannelEvent
} from '../models/channel-events/applications/application-selected.channel-event';
import {map} from 'rxjs/operators';
import {Application} from '../models/entities/application';

@Injectable()
export class ApplicationPageGuard implements CanActivate {

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(APPLICATION_SERVICE)
                     protected readonly _applicationService: IApplicationService) {
  }

  //#endregion

  //#region Methods

  public canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const queryParams = activatedRouteSnapshot.queryParams as ApplicationPageQueryParams;
    if (!queryParams?.applicationId) {
      this._messageBusService.addMessageInstance(new ApplicationSelectedChannelEvent(new Application('')));
      return of(true);
    }

    return this._applicationService.getByIdAsync(queryParams.applicationId)
      .pipe(
        tap(application => {
          this._messageBusService.addMessageInstance(new ApplicationSelectedChannelEvent(application));
        }),
        map(() => true)
      )
  }

  //#endregion

}
