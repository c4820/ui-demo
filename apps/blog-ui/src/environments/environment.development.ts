export class Environment {

  //#region Properties

  public static readonly production = false;

  public static readonly files = ['/assets/appsettings.json', '/assets/appsettings.Development.json'];

  //#endregion

}
