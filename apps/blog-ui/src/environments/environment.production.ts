export class Environment {

  //#region Properties

  public static readonly production = true;

  public static readonly files = ['/assets/appsettings.json', '/assets/appsettings.Production.json'];


  //#endregion

}
