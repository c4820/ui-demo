import {DialogService, IDialogService} from '@ui-tool/core';
import {Injectable, Injector} from '@angular/core';

@Injectable()
export class BasicDialogService extends DialogService {

  //#region Constructor

  public constructor(injector: Injector) {
    super(injector);
  }

  //#endregion
}
