import {IFeatureSentinelService, IRequirementHandler, REQUIREMENT_HANDLER} from '@ui-tool/core';
import {Inject, Injectable} from '@angular/core';
import {forkJoin, Observable, of, ReplaySubject, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class FeatureSentinelService implements IFeatureSentinelService {

  //#region Properties

  private __validationEvent$: Subject<void> = new ReplaySubject(1);

  //#endregion

  //#region Constructor

  public constructor(@Inject(REQUIREMENT_HANDLER)
                     protected readonly _requirementHandlers: IRequirementHandler[]) {
  }

//#endregion

  //#region Methods

  public ableToAccessFeaturesAsync(names: string[]): Observable<boolean> {
    const observables = names.map(name => {
      const requirementHandler = this._requirementHandlers
        .find(x => x.name === name);

      if (!requirementHandler) {
        return of(false);
      }

      return requirementHandler.shouldRequirementMetAsync();
    });

    return forkJoin(observables)
      .pipe(
        map(results => {
          const hasInvalidRequirement = results.findIndex(m => m !== true) !== -1;
          return hasInvalidRequirement;
        }),
        map(hasInvalidRequirement => {
          if (hasInvalidRequirement) {
            return false;
          }

          return true;
        })
      );
  }

  public doValidation(): void {
  }

  public hookValidationEventAsync(): Observable<void> {
    return this.__validationEvent$;
  }

  //#endregion

}
