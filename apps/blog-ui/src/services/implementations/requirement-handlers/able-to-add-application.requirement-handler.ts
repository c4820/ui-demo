import {IRequirementHandler} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {RequirementNames} from '../../../constants/requirement-names';

@Injectable()
export class AbleToAddApplicationRequirementHandler implements IRequirementHandler {

  //#region Properties

  public readonly name = RequirementNames.createApplication;

  //#endregion

  //#region Constructor

  public constructor() {
  }

  //#endregion

  //#region Methods

  public shouldRequirementMetAsync(): Observable<boolean> {
    debugger;
    return of(true);
  }

  //#endregion

}
