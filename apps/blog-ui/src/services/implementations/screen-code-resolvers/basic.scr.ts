import {DefaultScreenCodeResolver} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {ScreenCodes} from '../../../constants/screen-codes';

@Injectable()
export class BasicScr extends DefaultScreenCodeResolver {

  //#region Constructor

  public constructor() {

    const codeToUrl: { [key: string]: string } = {};
    super(codeToUrl);
  }

  //#endregion
}
