import {IPropertyService} from '../../interfaces/apis/property-service.interface';
import {Inject, Injectable} from '@angular/core';
import {Property} from '../../../models/entities/property';
import {ENDPOINT_RESOLVER} from '../../../constants/injectors';
import {IEndpointResolver} from '../../interfaces/endpoint-resolver.interface';
import {forkJoin, mergeMap, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AddPropertyViewModel} from '../../../view-models/properties/add-property.view-model';
import {map} from 'rxjs/operators';

@Injectable()
export class PropertyService implements IPropertyService {

  //#region Constructor

  public constructor(@Inject(ENDPOINT_RESOLVER)
                     protected readonly _endpointResolver: IEndpointResolver,
                     protected readonly _httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public getByIdAsync(id: string): Observable<Property> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/${id}`;
          return this._httpClient.get<Property>(fullUrl);
        })
      );
  }

  public getByMethodIdAsync(methodId: string): Observable<Property[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/by-method/${methodId}`;
          return this._httpClient.get<Property[]>(fullUrl);
        })
      );
  }

  public getByEventIdAsync(eventId: string): Observable<Property[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/by-event/${eventId}`;
          return this._httpClient.get<Property[]>(fullUrl);
        })
      );
  }


  public getByComponentIdAsync(componentId: string): Observable<Property[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/by-component/${componentId}`;
          return this._httpClient.get<Property[]>(fullUrl);
        })
      );
  }

  public addToComponentAsync(componentId: string,
                             model: AddPropertyViewModel): Observable<Property> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/to-component/${componentId}`;
          return this._httpClient.post<Property>(fullUrl, model);
        })
      );
  }

  public addToEventAsync(eventId: string, model: AddPropertyViewModel): Observable<Property> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/to-event/${eventId}`;
          return this._httpClient.post<Property>(fullUrl, model);
        })
      );
  }

  public addToMethodAsync(methodId: string, model: AddPropertyViewModel): Observable<Property> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/to-event/${methodId}`;
          return this._httpClient.post<Property>(fullUrl, model);
        })
      );
  }

  public deleteByIdAsync(propertyId: string): Observable<void> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/property/${propertyId}`;
          return this._httpClient.delete<void>(fullUrl);
        })
      );
  }

  public getEventIdToPropertiesAsync(eventIds: string[])
    : Observable<Record<string, Property[]>> {

    const getEventObservables: Observable<{
      eventId: string,
      properties: Property[]
    }>[] = [];

    for (const eventId of eventIds) {
      const getEventObservable = this.getByEventIdAsync(eventId)
        .pipe(
          map(properties => {
            return {
              eventId,
              properties
            };
          })
        );
      getEventObservables.push(getEventObservable);
    }

    return forkJoin(getEventObservables)
      .pipe(
        map(eventIdsToProperties => {
          const dictionary: Record<string, Property[]> = {};
          for (const item of eventIdsToProperties) {
            dictionary[item.eventId] = item.properties;
          }

          return dictionary;
        })
      );
  }

  //#endregion
}
