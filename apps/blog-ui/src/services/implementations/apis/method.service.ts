import {IMethodService} from '../../interfaces/apis/method-service.interface';
import {Inject, Injectable} from '@angular/core';
import {ENDPOINT_RESOLVER} from '../../../constants/injectors';
import {IEndpointResolver} from '../../interfaces/endpoint-resolver.interface';
import {HttpClient} from '@angular/common/http';
import {mergeMap, Observable} from 'rxjs';
import {Method} from '../../../models/entities/method';
import {AddMethodViewModel} from '../../../view-models/methods/add-method.view-model';

@Injectable()
export class MethodService implements IMethodService {

  //#region Constructor

  public constructor(@Inject(ENDPOINT_RESOLVER)
                     protected readonly _endpointResolver: IEndpointResolver,
                     protected readonly _httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public addAsync(command: AddMethodViewModel): Observable<Method> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/method`;
          return this._httpClient.post<Method>(fullUrl, command);
        })
      );
  }

  public getByIdAsync(id: string): Observable<Method> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/method/${id}`;
          return this._httpClient.get<Method>(fullUrl);
        })
      );
  }

  public getByComponentIdAsync(componentId: string): Observable<Method[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/method/by-component/${componentId}`;
          return this._httpClient.get<Method[]>(fullUrl);
        })
      );
  }

  public deleteByIdAsync(id: string): Observable<void> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/method/${id}`;
          return this._httpClient.delete<void>(fullUrl);
        })
      );
  }

  //#endregion
}
