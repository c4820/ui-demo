import {IComponentService} from '../../interfaces/apis/component-service.interface';
import {Inject, Injectable} from '@angular/core';
import {mergeMap, Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ENDPOINT_RESOLVER} from '../../../constants/injectors';
import {IEndpointResolver} from '../../interfaces/endpoint-resolver.interface';
import {Component} from '../../../models/entities/component';
import {AddComponentViewModel} from '../../../view-models/components/add-component.view-model';
import {ComponentKinds} from '../../../constants/component-kinds';

@Injectable()
export class ComponentService implements IComponentService {

  //#region Constructor

  public constructor(protected readonly _httpClient: HttpClient,
                     @Inject(ENDPOINT_RESOLVER) protected readonly _endpointResolver: IEndpointResolver) {
  }

  //#endregion

  //#region Methods

  public addAsync(command: AddComponentViewModel): Observable<Component> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/component`;
          return this._httpClient.post<Component>(fullUrl, command);
        })
      );
  }

  public getAvailableKindsAsync(): Observable<string[]> {
    return of([ComponentKinds.component, ComponentKinds.service]);
  }

  public getByApplicationIdAsync(applicationId: string, versionId: string): Observable<Component[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/component/by-application/${applicationId}?versionId=${versionId}`;
          return this._httpClient.get<Component[]>(fullUrl);
        })
      );
  }

  public getByIdAsync(id: string): Observable<Component> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/component/${id}`;
          return this._httpClient.get<Component>(fullUrl);
        })
      );
  }

  //#endregion

}
