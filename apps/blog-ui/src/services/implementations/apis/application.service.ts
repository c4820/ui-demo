import {IApplicationService} from '../../interfaces/apis/application-service.interface';
import {Inject, Injectable} from '@angular/core';
import {ENDPOINT_RESOLVER} from '../../../constants/injectors';
import {IEndpointResolver} from '../../interfaces/endpoint-resolver.interface';
import {mergeMap, Observable} from 'rxjs';
import {Application} from '../../../models/entities/application';
import {HttpClient} from '@angular/common/http';
import {AddApplicationViewModel} from '../../../view-models/applications/add-application.view-model';
import {EditApplicationViewModel} from '../../../view-models/applications/edit-application.view-model';

@Injectable()
export class ApplicationService implements IApplicationService {

  //#region Constructor

  public constructor(@Inject(ENDPOINT_RESOLVER)
                     protected readonly _endpointResolver: IEndpointResolver,
                     protected readonly _httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public addAsync(command: AddApplicationViewModel): Observable<Application> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application`;
          return this._httpClient.post<Application>(fullUrl, command);
        })
      );
  }

  public getAsync(): Observable<Application[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application`;
          return this._httpClient.get<Application[]>(fullUrl);
        })
      );
  }

  public getByIdAsync(id: string): Observable<Application> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application/${id}`;
          return this._httpClient.get<Application>(fullUrl);
        })
      );
  }


  public deleteByIdAsync(applicationId: string): Observable<void> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application/${applicationId}`;
          return this._httpClient.delete<void>(fullUrl);
        })
      );
  }

  public editAsync(applicationId: string,
                   command: EditApplicationViewModel): Observable<Application> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application/${applicationId}`;
          return this._httpClient.put<Application>(fullUrl, command);
        })
      );
  }

  //#endregion
}
