import {IEventService} from '../../interfaces/apis/event-service.interface';
import {Inject, Injectable} from '@angular/core';
import {mergeMap, Observable} from 'rxjs';
import {ENDPOINT_RESOLVER} from '../../../constants/injectors';
import {IEndpointResolver} from '../../interfaces/endpoint-resolver.interface';
import {HttpClient} from '@angular/common/http';
import {Event} from '../../../models/entities/event';
import {AddComponentEventViewModel} from '../../../view-models/events/add-component-event.view-model';

@Injectable()
export class EventService implements IEventService {

  //#region Constructor

  public constructor(@Inject(ENDPOINT_RESOLVER)
                     protected readonly _endpointResolver: IEndpointResolver,
                     protected readonly _httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public addAsync(command: AddComponentEventViewModel): Observable<Event> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/event`;
          return this._httpClient.post<Event>(fullUrl, command);
        })
      );
  }

  public getByIdAsync(id: string): Observable<Event> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/event/${id}`;
          return this._httpClient.get<Event>(fullUrl);
        })
      );
  }

  public getByComponentIdAsync(componentId: string): Observable<Event[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/event/by-component/${componentId}`;
          return this._httpClient.get<Event[]>(fullUrl);
        })
      );
  }

  public deleteByIdAsync(eventId: string): Observable<void> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/event/${eventId}`;
          return this._httpClient.delete<void>(fullUrl);
        })
      );
  }

  //#endregion

}
