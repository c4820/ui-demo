import {mergeMap, Observable} from 'rxjs';
import {ApplicationVersion} from '../../../models/entities/application-version';
import {
  AddApplicationVersionViewModel
} from '../../../view-models/application-versions/add-application-version.view-model';
import {
  EditApplicationVersionViewModel
} from '../../../view-models/application-versions/edit-application-version.view-model';
import {Inject, Injectable} from '@angular/core';
import {ENDPOINT_RESOLVER} from '../../../constants/injectors';
import {IEndpointResolver} from '../../interfaces/endpoint-resolver.interface';
import {IApplicationVersionService} from '../../interfaces/apis/application-version-service.interface';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ApplicationVersionService implements IApplicationVersionService {

  //#region Constructor

  public constructor(@Inject(ENDPOINT_RESOLVER)
                     protected readonly _endpointResolver: IEndpointResolver,
                     protected readonly _httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public getByApplicationIdAsync(applicationId: string): Observable<ApplicationVersion[]> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application-version/by-application-id/${applicationId}`;
          return this._httpClient.get<ApplicationVersion[]>(fullUrl);
        })
      );
  }

  public getByIdAsync(versionId: string): Observable<ApplicationVersion> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application-version/${versionId}`;
          return this._httpClient.get<ApplicationVersion>(fullUrl);
        })
      );
  }

  public addAsync(command: AddApplicationVersionViewModel): Observable<ApplicationVersion> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application-version`;
          return this._httpClient.post<ApplicationVersion>(fullUrl, command);
        })
      );
  }

  public editAsync(applicationId: string, command: EditApplicationVersionViewModel)
    : Observable<ApplicationVersion> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application-version/${applicationId}`;
          return this._httpClient.put<ApplicationVersion>(fullUrl, command);
        })
      );
  }

  public deleteByIdAsync(versionId: string): Observable<void> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/application-version/${versionId}`;
          return this._httpClient.delete<void>(fullUrl);
        })
      );
  }

  //#endregion

}
