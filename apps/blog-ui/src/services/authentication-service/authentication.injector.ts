import {InjectionToken} from '@angular/core';
import {IAuthenticationService} from './authentication-service.interface';

export const AUTHENTICATION_SERVICE = new InjectionToken<IAuthenticationService>('AUTHENTICATION_SERVICE');
