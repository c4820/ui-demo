import {Observable} from 'rxjs';
import {IAuthenticationResult} from '../../interfaces/idp/authentication-result.interface';

export interface IAuthenticationService {

  //#region Methods

  // Do to idp page asynchronously.
  goToIdpPageAsync(provider: string): Observable<void>;

  authenticateAsync(provider: string, code: string): Observable<IAuthenticationResult>;

  // Get the authentication result asynchronously.
  getAuthenticationResultAsync(): Observable<IAuthenticationResult>;

  deleteAuthenticationResultAsync(): Observable<void>;

  //#endregion

}
