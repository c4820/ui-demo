import {NgModule} from '@angular/core';
import {AUTHENTICATION_SERVICE} from './authentication.injector';
import {AuthenticationService} from './authentication.service';

@NgModule({
  providers: [
    {
      provide: AUTHENTICATION_SERVICE,
      useClass: AuthenticationService
    }
  ]
})
export class AuthenticationServiceModule {

}
