import {IAuthenticationService} from './authentication-service.interface';
import {Inject, Injectable} from '@angular/core';
import {mergeMap, Observable, of, tap} from 'rxjs';
import {APP_SETTINGS_SERVICE, ENDPOINT_RESOLVER} from '../../constants/injectors';
import {IAppSettingsService} from '../interfaces/app-settings-service.interface';
import {map} from 'rxjs/operators';
import {WINDOW} from '@ui-tool/core';
import {IIdpOptions} from '../../interfaces/idp/idp-options.interface';
import {IAuthenticationResult} from '../../interfaces/idp/authentication-result.interface';
import {IEndpointResolver} from '../interfaces/endpoint-resolver.interface';
import {HttpClient} from '@angular/common/http';
import {AuthenticationRequestViewModel} from '../../view-models/authentication/authentication-request.view-model';
import {LocalStorageService} from 'ngx-webstorage';
import {StorageKeys} from '../../constants/storage-keys';

@Injectable()
export class AuthenticationService implements IAuthenticationService {

  //#region Constructor

  public constructor(@Inject(APP_SETTINGS_SERVICE)
                     protected readonly _appSettingsService: IAppSettingsService,
                     @Inject(ENDPOINT_RESOLVER)
                     protected readonly _endpointResolver: IEndpointResolver,
                     @Inject(WINDOW)
                     protected readonly _window: Window,
                     protected readonly _storage: LocalStorageService,
                     protected readonly _httpClient: HttpClient) {
  }

//#endregion

  //#region Methods

  public goToIdpPageAsync(provider: string): Observable<void> {
    return this._getIdpOptionsAsync(provider)
      .pipe(
        tap(option => {
          const queryStrings = new URLSearchParams();
          queryStrings.set('response_type', 'code');
          queryStrings.set('client_id', option!.clientId);
          queryStrings.set('redirect_uri', `${option!.redirectUrl}?provider=${provider}`);
          queryStrings.set('scope', option!.scope);

          const url = `${option!.baseUrl}?${queryStrings.toString()}`;
          this._window.location.href = url;
        }),
        map(() => void(0))
      );
  }

  public authenticateAsync(provider: string, code: string): Observable<IAuthenticationResult> {
    return this._getIdpOptionsAsync(provider)
      .pipe(
        mergeMap(option => {
          return this._endpointResolver
            .loadEndpointAsync('', '')
            .pipe(
              mergeMap(baseUrl => {
                const apiUrl = `${baseUrl}/oauth/authorize`;
                return this._httpClient
                  .post<IAuthenticationResult>(apiUrl, new AuthenticationRequestViewModel(provider, code, option.redirectUrl));
              })
            );
        })
      );
  }

  public deleteAuthenticationResultAsync(): Observable<void> {
    this._storage.clear(StorageKeys.authenticationResult);
    return of(void(0));
  }

  public getAuthenticationResultAsync(): Observable<IAuthenticationResult> {
    const authenticationResult = this._storage.retrieve(StorageKeys.authenticationResult) as IAuthenticationResult;
    return of(authenticationResult);
  }

  //#endregion

  //#region Internal methods

  protected _getIdpOptionsAsync(provider: string): Observable<IIdpOptions> {
    return this._appSettingsService
      .getAppSettingsAsync()
      .pipe(
        map(appSettings => appSettings.idpOptions),
        map(idpOptions => {
          const options = idpOptions.find(x => x.provider === provider);
          if (!options) {
            throw new Error('IDP_OPTIONS_NOT_FOUND');
          }

          return options;
        })
      );
  }

  //#endregion

}
