import {Observable} from 'rxjs';
import {ProfileViewModel} from '../../view-models/profiles/profile.view-model';

export interface IProfileService {

  //#region Methods

  getAsync(): Observable<ProfileViewModel>;

  //#endregion

}
