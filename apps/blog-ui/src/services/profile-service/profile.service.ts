import {IProfileService} from './profile-service.interface';
import {Inject, Injectable} from '@angular/core';
import {mergeMap, Observable} from 'rxjs';
import {ProfileViewModel} from '../../view-models/profiles/profile.view-model';
import {HttpClient} from '@angular/common/http';
import {ENDPOINT_RESOLVER} from '../../constants/injectors';
import {IEndpointResolver} from '../interfaces/endpoint-resolver.interface';

@Injectable()
export class ProfileService implements IProfileService {

  //#region Constructor

  public constructor(
    @Inject(ENDPOINT_RESOLVER)
    protected readonly _endpointResolver: IEndpointResolver,
    protected readonly _httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public getAsync(): Observable<ProfileViewModel> {
    return this._endpointResolver.loadEndpointAsync('', '')
      .pipe(
        mergeMap(apiUrl => {
          const fullUrl = `${apiUrl}/api/profile`;
          return this._httpClient.get<ProfileViewModel>(fullUrl);
        })
      );
  }

  //#endregion

}
