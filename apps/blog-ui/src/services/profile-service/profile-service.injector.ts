import {InjectionToken} from '@angular/core';
import {IProfileService} from './profile-service.interface';

export const PROFILE_SERVICE = new InjectionToken<IProfileService>('PROFILE_SERVICE');
