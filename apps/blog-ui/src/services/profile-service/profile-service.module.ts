import {NgModule} from '@angular/core';
import {PROFILE_SERVICE} from './profile-service.injector';
import {ProfileService} from './profile.service';

@NgModule({
  providers: [
    {
      provide: PROFILE_SERVICE,
      useClass: ProfileService
    }
  ]
})
export class ProfileServiceModule {
}
