import {Observable} from 'rxjs';
import {Application} from '../../../models/entities/application';
import {AddApplicationViewModel} from '../../../view-models/applications/add-application.view-model';
import {EditApplicationViewModel} from '../../../view-models/applications/edit-application.view-model';

export interface IApplicationService {

  //#region Methods

  addAsync(command: AddApplicationViewModel): Observable<Application>;

  editAsync(applicationId: string, command: EditApplicationViewModel)
    : Observable<Application>;

  // Get all applications in the system.
  getAsync(): Observable<Application[]>;

  // Get by id asynchronously.
  getByIdAsync(id: string): Observable<Application>;

  deleteByIdAsync(applicationId: string): Observable<void>;

  //#endregion

}
