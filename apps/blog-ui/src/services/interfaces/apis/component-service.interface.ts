import {Observable} from 'rxjs';
import {Component} from '../../../models/entities/component';
import {AddComponentViewModel} from '../../../view-models/components/add-component.view-model';

export interface IComponentService {

  //#region Methods

  addAsync(command: AddComponentViewModel): Observable<Component>;

  getAvailableKindsAsync(): Observable<string[]>;

  getByIdAsync(id: string): Observable<Component>;

  getByApplicationIdAsync(applicationId: string, versionId: string): Observable<Component[]>;

  //#endregion

}
