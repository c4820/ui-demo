import {Observable} from 'rxjs';
import {Method} from '../../../models/entities/method';
import {AddMethodViewModel} from '../../../view-models/methods/add-method.view-model';

export interface IMethodService {

  //#region Methods

  addAsync(command: AddMethodViewModel): Observable<Method>;

  getByIdAsync(id: string): Observable<Method>;

  // Get by component id asynchronously.
  getByComponentIdAsync(componentId: string): Observable<Method[]>;

  deleteByIdAsync(id: string): Observable<void>;

  //#endregion

}
