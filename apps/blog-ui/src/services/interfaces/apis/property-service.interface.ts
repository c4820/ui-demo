import {Property} from '../../../models/entities/property';
import {Observable} from 'rxjs';
import {AddPropertyViewModel} from '../../../view-models/properties/add-property.view-model';

export interface IPropertyService {

  //#region Methods

  getByIdAsync(id: string): Observable<Property>;

  getByMethodIdAsync(methodId: string): Observable<Property[]>;

  getByEventIdAsync(eventId: string): Observable<Property[]>;

  getByComponentIdAsync(componentId: string): Observable<Property[]>;

  addToComponentAsync(componentId: string, model: AddPropertyViewModel): Observable<Property>;

  addToMethodAsync(methodId: string, model: AddPropertyViewModel): Observable<Property>;

  addToEventAsync(eventId: string, model: AddPropertyViewModel): Observable<Property>;

  deleteByIdAsync(propertyId: string): Observable<void>;

  getEventIdToPropertiesAsync(eventIds: string[]): Observable<Record<string, Property[]>>;

  //#endregion

}
