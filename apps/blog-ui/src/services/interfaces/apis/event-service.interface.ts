import {Observable} from 'rxjs';
import {Event} from '../../../models/entities/event';
import {AddComponentEventViewModel} from '../../../view-models/events/add-component-event.view-model';

export interface IEventService {

  //#region Methods

  addAsync(command: AddComponentEventViewModel): Observable<Event>;

  getByIdAsync(id: string): Observable<Event>;

  getByComponentIdAsync(componentId: string): Observable<Event[]>;

  deleteByIdAsync(eventId: string): Observable<void>;

  //#endregion

}
