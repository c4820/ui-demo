import {mergeMap, Observable} from 'rxjs';
import {ApplicationVersion} from '../../../models/entities/application-version';
import {
  AddApplicationVersionViewModel
} from '../../../view-models/application-versions/add-application-version.view-model';
import {
  EditApplicationVersionViewModel
} from '../../../view-models/application-versions/edit-application-version.view-model';

export interface IApplicationVersionService {

  //#region Methods

  getByApplicationIdAsync(applicationId: string): Observable<ApplicationVersion[]>;

  getByIdAsync(versionId: string): Observable<ApplicationVersion>;

  addAsync(command: AddApplicationVersionViewModel): Observable<ApplicationVersion>;

  editAsync(applicationId: string, command: EditApplicationVersionViewModel): Observable<ApplicationVersion>;

  deleteByIdAsync(versionId: string): Observable<void>;

  //#endregion

}
