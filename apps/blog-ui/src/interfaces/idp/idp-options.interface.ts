export interface IIdpOptions {

  //#region Properties

  provider: string;

  baseUrl: string;

  clientId: string;

  redirectUrl: string;

  scope: string;

  //#endregion

}
