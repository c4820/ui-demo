export interface IAuthenticationResult {

  //#region Properties

  accessToken: string;

  refreshToken: string;

  //#endregion
}
