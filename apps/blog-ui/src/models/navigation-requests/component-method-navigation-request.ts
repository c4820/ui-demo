import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ComponentMethodPageQueryParams} from '../query-params/component-method-page-query-params';

export class ComponentMethodNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  constructor(applicationId: string, versionId: string, componentId: string, methodId?: string) {
    super(ScreenCodes.componentMethod, void (0), {
      queryParams: new ComponentMethodPageQueryParams(applicationId, versionId, componentId, methodId)
    });
  }

  //#endregion

}
