import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ApplicationComponentPageQueryParams} from '../query-params/application-component-page-query-params';

export class ComponentMethodsNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  constructor(applicationId: string, versionId: string, componentId: string, methodId?: string) {
    super(ScreenCodes.componentMethods, void (0), {
      queryParams: new ApplicationComponentPageQueryParams(applicationId, versionId, componentId)
    });
  }

  //#endregion

}
