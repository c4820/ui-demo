import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';

export class HomeNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructors

  public constructor() {
  super(ScreenCodes.home, void(0), {});
}

//#endregion

}
