import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ComponentPropertyPageQueryParams} from '../query-params/component-property-page-query-params';

export class ComponentPropertyNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  public constructor(public readonly applicationId: string, public readonly versionId: string,
                     public readonly componentId: string,
                     public readonly hostKind: string,
                     public readonly hostId: string,
                     public readonly propertyId?: string) {
    super(ScreenCodes.componentProperty, void (0), {
      queryParams: new ComponentPropertyPageQueryParams(applicationId,
        versionId,
        componentId,
        hostKind,
        hostId,
        propertyId)
    });
  }

  //#endregion
}
