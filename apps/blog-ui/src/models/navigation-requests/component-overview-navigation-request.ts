import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ComponentOverviewPageQueryParams} from '../query-params/component-overview-page-query-params';

export class ComponentOverviewNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  public constructor(applicationId: string, versionId: string, componentId?: string) {
    super(ScreenCodes.applicationVersion, void (0), {
      queryParams: new ComponentOverviewPageQueryParams(applicationId, versionId, componentId)
    });

    //#endregion

  }
}
