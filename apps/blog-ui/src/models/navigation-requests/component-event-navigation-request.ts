import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ComponentEventPageQueryParams} from '../query-params/component-event-page-query-params';

export class ComponentEventNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  public constructor(applicationId: string, versionId: string,
                     componentId: string, eventId?: string) {
    super(ScreenCodes.componentEvent, void (0), {
      queryParams: new ComponentEventPageQueryParams(applicationId, versionId, componentId, eventId)
    });
  }

  //#endregion

}
