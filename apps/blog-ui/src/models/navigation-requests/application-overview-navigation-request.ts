import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ApplicationOverviewQueryParams} from '../query-params/application-overview-query-params';

export class ApplicationOverviewNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  public constructor(applicationId?: string) {
    super(ScreenCodes.applicationOverview, void (0), {
      queryParams: applicationId ? new ApplicationOverviewQueryParams(applicationId) : {}
    });
  }

  //#endregion

}
