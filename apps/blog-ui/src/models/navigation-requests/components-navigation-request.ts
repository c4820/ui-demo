import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ApplicationComponentsPageQueryParams} from '../query-params/application-components-page-query-params';

export class ComponentsNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  public constructor(applicationId: string, versionId: string) {
    super(ScreenCodes.applicationComponents, void(0), {
      queryParams: new ApplicationComponentsPageQueryParams(applicationId, versionId)
    });
  }

  //#endregion

}
