import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ApplicationVersionQueryParams} from '../query-params/application-version-query-params';

export class ApplicationVersionNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructors

  public constructor(applicationId: string, versionId?: string) {
    super(ScreenCodes.applicationVersion, void(0), {
      queryParams: new ApplicationVersionQueryParams(applicationId, versionId)
    });
  }

//#endregion

}
