import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '../../constants/screen-codes';
import {ComponentEventsPageQueryParams} from '../query-params/component-events-page-query-params';

export class ComponentEventsNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  constructor(public readonly applicationId: string,
              public readonly versionId: string, public readonly componentId: string) {
    super(ScreenCodes.componentEvents, void (0), {
      queryParams: new ComponentEventsPageQueryParams(applicationId, versionId, componentId)
    });
  }

//#endregion

}
