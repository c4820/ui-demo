import {IIdpOptions} from '../interfaces/idp/idp-options.interface';

export class AppSettings {

  //#region Properties

  public baseUrl!: string;

  public idpOptions!: IIdpOptions[];

  //#endregion

}
