import {DefaultEntity} from './default-entity';

export class Method extends DefaultEntity {

  //#region Properties

  public name!: string;

  public description?: string;

  public valueType!: string;

  //#endregion

  //#region Constructor

  public constructor(public readonly id: string) {
    super();
  }

  //#endregion

}
