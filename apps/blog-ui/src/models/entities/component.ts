import {DefaultEntity} from './default-entity';

export class Component extends DefaultEntity {

  //#region Properties

  public name!: string;

  public kind!: string;

  public description?: string;

  public optional?: boolean;

  //#endregion

  //#region Constructor

  public constructor(public readonly id: string) {
    super();
  }

  //#endregion

}
