export class ApplicationVersion {

  //#region Constructor

  public constructor(public readonly id: string,
                     public readonly title: string,
                     public readonly description: string,
                     public readonly changeLogs?: string[]) {
  }

  //#endregion

}
