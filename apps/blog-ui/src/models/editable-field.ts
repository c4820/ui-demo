export class EditableField<T> {

  //#region Constructor

  public constructor(public readonly value: T,
                     public readonly hasModified?: boolean) {

  }

  //#endregion

}
