import {Application} from '../entities/application';
import {ApplicationVersion} from '../entities/application-version';
import {Component} from '../entities/component';
import {MessageChannels} from '../../constants/channel-events/message-channels';
import {PropertyMessageEvents} from '../../constants/channel-events/property-message-events';
import {Property} from '../entities/property';
import {MessageHook} from '@message-bus/core';

@MessageHook(MessageChannels.component, PropertyMessageEvents.propertyChosen)
export class ComponentPropertyChosenChannelEvent {


  //#region Constructor

  public constructor(public readonly application: Application,
                     public readonly version: ApplicationVersion,
                     public readonly component: Component,
                     public readonly property: Property,
                     public readonly hostKind?: string,
                     public hostId?: string) {
  }

  //#endregion

}
