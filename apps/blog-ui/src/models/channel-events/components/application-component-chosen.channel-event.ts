import {Application} from '../../entities/application';
import {ApplicationVersion} from '../../entities/application-version';
import {Component} from '../../entities/component';
import {MessageHook} from '@message-bus/core';
import {MessageChannels} from "../../../constants/channel-events/message-channels";
import {ComponentMessageEvents} from "../../../constants/channel-events/component-message-events";

@MessageHook(MessageChannels.component, ComponentMessageEvents.componentChosen)
export class ApplicationComponentChosenChannelEvent {


  //#region Constructor

  public constructor(public readonly application: Application,
                     public readonly version: ApplicationVersion,
                     public readonly component: Component) {
  }

  //#endregion

}
