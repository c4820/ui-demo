import {Application} from '../../entities/application';
import {ApplicationVersion} from '../../entities/application-version';
import {Component} from '../../entities/component';
import {MessageChannels} from '../../../constants/channel-events/message-channels';
import {ComponentMessageEvents} from '../../../constants/channel-events/component-message-events';
import {Event} from '../../entities/event';
import {MessageHook} from '@message-bus/core';

@MessageHook(MessageChannels.component, ComponentMessageEvents.componentChosen)
export class ApplicationComponentEventChosenChannelEvent {

  //#region Constructor

  public constructor(public readonly application: Application,
                     public readonly version: ApplicationVersion,
                     public readonly component: Component,
                     public readonly event: Event) {
  }

  //#endregion

}
