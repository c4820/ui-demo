import {MessageHook} from '@message-bus/core';
import {Application} from '../../entities/application';
import {ApplicationVersion} from '../../entities/application-version';
import {Component} from '../../entities/component';
import {MessageChannels} from '../../../constants/channel-events/message-channels';
import {Method} from '../../entities/method';
import {MethodMessageEvents} from '../../../constants/channel-events/method-message-events';

@MessageHook(MessageChannels.component, MethodMessageEvents.methodChosen)
export class ComponentMethodChosenChannelEvent {

  //#region Constructor

  public constructor(public readonly  application: Application,
                     public readonly version: ApplicationVersion,
                     public readonly component: Component,
                     public readonly method: Method) {
  }

  //#endregion

}
