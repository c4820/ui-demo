import {MessageHook} from '@message-bus/core';
import {ProfileViewModel} from '../../../view-models/profiles/profile.view-model';
import {MessageChannels} from '../../../constants/channel-events/message-channels';
import {ProfileMessageEvents} from '../../../constants/channel-events/profile-message-events';

@MessageHook(MessageChannels.profile, ProfileMessageEvents.updated)
export class ProfileUpdatedChannelEvent {

  //#region Constructor

  public constructor(public readonly profile?: ProfileViewModel) {
  }

  //#endregion
}
