import {MessageHook} from '@message-bus/core';
import {MessageChannels} from '../../../constants/channel-events/message-channels';
import {NavbarEvents} from '../../../constants/channel-events/navbar-events';

@MessageHook(MessageChannels.navbar, NavbarEvents.changeTitle)
export class ChangeNavbarTitleChannelEvent {

  //#region Constructor

  public constructor(public readonly title: string) {
  }

  //#endregion
}
