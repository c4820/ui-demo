import {MessageHook} from '@message-bus/core';
import {Application} from '../../entities/application';
import {MessageChannels} from '../../../constants/channel-events/message-channels';
import {ApplicationMessageEvents} from '../../../constants/channel-events/application-message-events';
import {ApplicationVersion} from '../../entities/application-version';

@MessageHook(MessageChannels.application, ApplicationMessageEvents.applicationVersionSelected)
export class ApplicationVersionSelectedChannelEvent {

  //#region Properties

  //#endregion

  //#region Constructor

  public constructor(public readonly application: Application,
                     public readonly version?: ApplicationVersion) {
  }

  //#endregion

}
