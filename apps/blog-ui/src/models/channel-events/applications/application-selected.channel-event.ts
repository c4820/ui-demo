import {Application} from '../../entities/application';
import {MessageChannels} from '../../../constants/channel-events/message-channels';
import {ApplicationMessageEvents} from '../../../constants/channel-events/application-message-events';
import {MessageHook} from '@message-bus/core';

@MessageHook(MessageChannels.application, ApplicationMessageEvents.applicationSelected)
export class ApplicationSelectedChannelEvent {

  //#region Constructor

  public constructor(public readonly application: Application) {
  }

  //#endregion

}
