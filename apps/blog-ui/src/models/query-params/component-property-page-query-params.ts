import {ComponentPropertiesPageQueryParams} from './component-properties-page-query-params';

export class ComponentPropertyPageQueryParams extends ComponentPropertiesPageQueryParams {

  //#region Constructor

  public constructor(applicationId: string, versionId: string,
                     componentId: string,
                     public readonly hostKind: string,
                     public readonly hostId: string,
                     public readonly propertyId?: string) {
    super(applicationId, versionId, componentId);
  }

//#endregion

}
