import {ComponentEventsPageQueryParams} from './component-events-page-query-params';

export class ComponentEventPageQueryParams extends ComponentEventsPageQueryParams {

  //#region Constructor

  public constructor(applicationId: string, versionId: string,
                     componentId: string, public readonly eventId?: string) {
    super(applicationId, versionId, componentId);
  }

//#endregion

}
