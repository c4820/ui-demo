import {ApplicationComponentsPageQueryParams} from './application-components-page-query-params';

export class ApplicationComponentPageQueryParams extends ApplicationComponentsPageQueryParams {

  //#region Constructor

  constructor(applicationId: string, versionId: string,
              public readonly componentId: string) {
    super(applicationId, versionId);
  }

//#endregion

}
