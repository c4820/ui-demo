export class ApplicationPageQueryParams {

  //#region Constructor

  public constructor(public readonly applicationId: string, public readonly versionId: string) {
  }

  //#endregion

}
