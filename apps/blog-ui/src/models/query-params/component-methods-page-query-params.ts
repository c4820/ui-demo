import {ApplicationComponentsPageQueryParams} from './application-components-page-query-params';

export class ComponentMethodsPageQueryParams extends ApplicationComponentsPageQueryParams {

  //#region Constructor

  public constructor(applicationId: string, versionId: string,
                     public readonly componentId: string) {
    super(applicationId, versionId);
  }

  //#endregion
}
