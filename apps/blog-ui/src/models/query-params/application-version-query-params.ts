export class ApplicationVersionQueryParams {

  //#region Constructor

  public constructor(public readonly applicationId: string,
                     public readonly versionId?: string) {
  }

  //#endregion

}
