export class ApplicationOverviewQueryParams {

  //#region Constructor

  public constructor(public readonly applicationId: string) {
  }

  //#endregion

}
