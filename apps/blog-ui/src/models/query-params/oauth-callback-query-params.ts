export class OauthCallbackQueryParams {

  //#region Constructor

  public constructor(public readonly provider: string,
                     public readonly code: string) {
  }

  //#endregion

}
