import {ApplicationComponentsPageQueryParams} from './application-components-page-query-params';

export class ComponentEventsPageQueryParams extends ApplicationComponentsPageQueryParams {

  //#region Constructor

  public constructor(applicationId: string, versionId: string,
                     public readonly componentId: string) {
    super(applicationId, versionId);
  }

  //#endregion
}
