import {ApplicationPageQueryParams} from './application-page-query-params';

export class ApplicationComponentsPageQueryParams extends ApplicationPageQueryParams {

  //#region Constructor

  constructor(applicationId: string, versionId: string) {
    super(applicationId, versionId);
  }

  //#endregion

}
