import {ApplicationComponentsPageQueryParams} from './application-components-page-query-params';

export class ComponentPropertiesPageQueryParams extends ApplicationComponentsPageQueryParams {

  //#region Constructor

  public constructor(applicationId: string, versionId: string,
                     public readonly componentId: string) {
    super(applicationId, versionId);
  }

  //#endregion
}
