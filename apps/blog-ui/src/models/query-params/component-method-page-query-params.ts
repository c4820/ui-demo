import {ComponentMethodsPageQueryParams} from './component-methods-page-query-params';

export class ComponentMethodPageQueryParams extends ComponentMethodsPageQueryParams {

  //#region Constructor

  constructor(applicationId: string, versionId: string, componentId: string,
              public readonly methodId?: string) {
    super(applicationId, versionId, componentId);
  }

//#endregion

}
