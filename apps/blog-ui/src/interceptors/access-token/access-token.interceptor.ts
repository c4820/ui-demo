import { Inject, Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpStatusCode,
} from '@angular/common/http';
import { catchError, Observable, throwError, mergeMap } from 'rxjs';
import { ISmartNavigatorService, SMART_NAVIGATOR_SERVICE } from '@ui-tool/core';
import { DOCUMENT } from '@angular/common';
import {LocalStorageService} from 'ngx-webstorage';
import {StorageKeys} from '../../constants/storage-keys';
import {IAuthenticationResult} from '../../interfaces/idp/authentication-result.interface';

@Injectable()
export class AccessTokenInterceptor implements HttpInterceptor {

  //#region Constructor

  public constructor(
    @Inject(SMART_NAVIGATOR_SERVICE)
    protected readonly _navigationService: ISmartNavigatorService,
    @Inject(DOCUMENT) protected readonly _document: Document,
    protected readonly _storage: LocalStorageService
  ) {}

  //#endregion

  //#region Methods

  public intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {

    const authenticationResult = this._storage.retrieve(StorageKeys.authenticationResult) as IAuthenticationResult;
    const headers: Record<string, string> = {};
    const accessToken = authenticationResult?.accessToken;
    if (accessToken != null && accessToken.length > 0) {
      headers['Authorization'] = `Bearer ${accessToken}`;
    }

    const clonedRequest = request.clone({
      reportProgress: true,
      setHeaders: headers
    });

    return next.handle(clonedRequest)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === HttpStatusCode.Unauthorized) {
             this._storage.clear(StorageKeys.authenticationResult)
          }
          return throwError(() => error);
        })
      );

  }

  //#endregion
}
