import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AccessTokenInterceptor } from './access-token.interceptor';
import { SmartNavigatorModule } from '@ui-tool/core';

@NgModule({
  imports: [SmartNavigatorModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AccessTokenInterceptor,
    },
  ],
})
export class AccessTokenInterceptorModule {}
