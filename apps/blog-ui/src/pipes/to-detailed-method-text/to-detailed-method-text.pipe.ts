import {Pipe, PipeTransform} from '@angular/core';
import {Method} from '../../models/entities/method';
import {Property} from '../../models/entities/property';

@Pipe({
  name: 'toDetailedMethodText'
})
export class ToDetailedMethodTextPipe implements PipeTransform {

  //#region Methods

  public transform(method: Method, properties: Property[]): string {
    const szMethodPropertyTitles = (properties || []).map(property => {
      return `<i>${property.name}</i>: <span class="text-info">${method.valueType}</span>`;
    });

    const szMethodPropertiesTitle = szMethodPropertyTitles.join(',');

    return `<code>${method.name}</code>(${szMethodPropertiesTitle}): <b class="badge bg-info">${method.valueType}</b>`;
  }

  //#endregion

}
