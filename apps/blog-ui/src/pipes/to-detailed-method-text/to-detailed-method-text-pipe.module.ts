import {NgModule} from '@angular/core';
import {ToDetailedMethodTextPipe} from './to-detailed-method-text.pipe';

@NgModule({
  declarations: [
    ToDetailedMethodTextPipe
  ],
  exports: [
    ToDetailedMethodTextPipe
  ]
})
export class ToDetailedMethodTextPipeModule {
}
