import {Pipe, PipeTransform} from '@angular/core';
import {Property} from '../../models/entities/property';
import {Event} from '../../models/entities/event';

@Pipe({
  name: 'toDetailedEventText'
})
export class ToDetailedEventTextPipe implements PipeTransform {

  //#region Methods

  public transform(event: Event, properties: Property[]): string {
    const szMethodPropertyTitles = (properties || []).map(property => {
      return `<i class="text-decoration-underline">${property.name}</i>: <span class="text-info">${property.valueType}</span>`;
    });

    const szMethodPropertiesTitle = szMethodPropertyTitles.join(', ');

    return `<code>${event.name}</code>(${szMethodPropertiesTitle})`;
  }

  //#endregion

}
