import {NgModule} from '@angular/core';
import {ToDetailedEventTextPipe} from './to-detailed-event-text.pipe';

@NgModule({
  declarations: [
    ToDetailedEventTextPipe
  ],
  exports: [
    ToDetailedEventTextPipe
  ]
})
export class ToDetailedEventTextPipeModule {
}
