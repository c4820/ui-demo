import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddPropertyViewModel {

  //#region Constructor

  public constructor(public readonly name: string,
                     public readonly description: string,
                     public readonly valueType: string,
                     public readonly defaultValue: string,
                     public readonly optional: boolean,
                     public readonly availability: ItemAvailabilities) {
  }

  //#endregion

}
