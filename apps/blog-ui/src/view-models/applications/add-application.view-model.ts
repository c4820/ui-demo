export class AddApplicationViewModel {

  //#region Constructor

  public constructor(public readonly name: string,
                     public readonly description: string) {
  }

  //#endregion

}
