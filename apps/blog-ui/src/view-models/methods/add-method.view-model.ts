import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddMethodViewModel {

  //#region Methods

  public constructor(public readonly componentId: string,
                     public readonly name: string,
                     public readonly description: string,
                     public readonly valueType: string,
                     public readonly availability: ItemAvailabilities) {
  }

  //#endregion

}
