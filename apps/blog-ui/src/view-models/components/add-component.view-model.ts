import {ItemAvailabilities} from '../../enumerations/item-availabilities';

export class AddComponentViewModel {

  //#region Properties

  //#endregion

  //#region Constructor

  public constructor(public readonly applicationId: string,
                     public readonly versionId: string,
                     public readonly name: string,
                     public readonly kind: string,
                     public readonly description: string,
                     public readonly availability: ItemAvailabilities) {
  }

  //#endregion

}
