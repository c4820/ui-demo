export class AddApplicationVersionViewModel {

  //#region Constructor

  public constructor(
    public readonly applicationId: string,
    public readonly title: string,
    public readonly description: string) {
  }

  //#endregion

}
