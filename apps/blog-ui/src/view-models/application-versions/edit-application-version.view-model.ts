import {EditableField} from '../../models/editable-field';

export class EditApplicationVersionViewModel {

  //#region Properties

  public title?: EditableField<string>;

  public description?: EditableField<string>;

  //#endregion
}
