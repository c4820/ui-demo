export class AuthenticationRequestViewModel {

  //#region Properties

  public readonly provider: string;

  public readonly code: string;

  public readonly redirectUri: string;

  //#endregion

  //#region Constructor

  public constructor(provider: string, code: string, redirectUri: string) {
    this.provider = provider;
    this.code = code;
    this.redirectUri = redirectUri;
  }

  //#endregion

}
