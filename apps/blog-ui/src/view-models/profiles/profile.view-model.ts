export class ProfileViewModel {

  //#region Properties

  public picture = '';

  public email = '';

  public verified = false;

  //#endregion

  //#region Constructor

  public constructor(public readonly id: string,
                     public readonly issuedAt: Date,
                     public readonly expiryTime: Date) {
  }

  //#endregion
}
