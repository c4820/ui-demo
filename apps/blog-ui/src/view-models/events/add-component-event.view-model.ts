export class AddComponentEventViewModel {

  //#region Constructor

  public constructor(public readonly componentId: string,
                     public readonly name: string,
                     public readonly description: string) {
  }

  //#endregion

}
