import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainLayoutModule} from './main-layout/main-layout.module';
import {
  FEATURE_SENTINEL_SERVICE,
  FeatureSentinelModule,
  REQUIREMENT_HANDLER,
  SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
  SmartNavigatorModule
} from '@ui-tool/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {
  BasicScr
} from '../services/implementations/screen-code-resolvers/basic.scr';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpLoaderFactory} from '../factories/translate.factory';
import {ApplicationScr} from '../services/implementations/application.scr';
import {loadAppSettings} from '../factories/appsettings.factory';
import {
  APP_SETTINGS_SERVICE,
  APPLICATION_SERVICE, APPLICATION_VERSION_SERVICE,
  COMPONENT_SERVICE,
  ENDPOINT_RESOLVER, EVENT_SERVICE, METHOD_SERVICE,
  PROPERTY_SERVICE
} from '../constants/injectors';
import {AppSettingsService} from '../services/implementations/app-settings.service';
import {MessageBusModule} from '@message-bus/core';
import {ScreenCodes} from '../constants/screen-codes';
import {ApplicationService} from '../services/implementations/apis/application.service';
import {EndpointResolver} from '../services/implementations/endpoint.resolver';
import {ComponentService} from '../services/implementations/apis/component.service';
import {PropertyService} from '../services/implementations/apis/property.service';
import {EventService} from '../services/implementations/apis/event.service';
import {MethodService} from '../services/implementations/apis/method.service';
import {FeatureSentinelService} from '../services/implementations/requirement-handlers/feature-sentinel.service';
import {ApplicationPageGuard} from '../guards/application-page.guard';
import {ApplicationVersionService} from '../services/implementations/apis/application-version.service';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {AuthenticationServiceModule} from '../services/authentication-service/authentication-service.module';
import {AccessTokenInterceptor, AccessTokenInterceptorModule} from '../interceptors/access-token';
import {ProfileGuardModule} from '../guards/profile-guard/profile-guard.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SmartNavigatorModule.forRoot({
      [ScreenCodes.home]: '/',
      [ScreenCodes.applicationOverview]: '/application/overview',
      [ScreenCodes.applicationVersion]: '/application/component/version',
      [ScreenCodes.application]: '/application',
      [ScreenCodes.applicationComponents]: '/application/component',
      [ScreenCodes.componentOverview]: '/application/component/overview',
      [ScreenCodes.componentProperty]: '/application/component/property',
      [ScreenCodes.componentProperties]: '/application/component/properties',
      [ScreenCodes.componentMethods]: '/application/component/methods',
      [ScreenCodes.componentMethod]: '/application/component/method',
      [ScreenCodes.componentEvents]: '/application/component/events',
      [ScreenCodes.componentEvent]: '/application/component/event'
    }),
    ProfileGuardModule,
    AccessTokenInterceptorModule,
    AuthenticationServiceModule,
    MainLayoutModule,
    AppRoutingModule,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxWebstorageModule.forRoot(),
    MessageBusModule.forRoot()
  ],
  providers: [
    {
      provide: APP_SETTINGS_SERVICE,
      useClass: AppSettingsService
    },
    {
      provide: APP_INITIALIZER,
      useFactory: loadAppSettings,
      deps: [APP_SETTINGS_SERVICE],
      multi: true
    },
    {
      provide: SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
      useClass: ApplicationScr,
      multi: true
    },
    {
      provide: SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
      useClass: BasicScr,
      multi: true
    },
    {
      provide: ENDPOINT_RESOLVER,
      useClass: EndpointResolver
    },
    {
      provide: APPLICATION_SERVICE,
      useClass: ApplicationService
    },
    {
      provide: COMPONENT_SERVICE,
      useClass: ComponentService
    },
    {
      provide: PROPERTY_SERVICE,
      useClass: PropertyService
    },
    {
      provide: EVENT_SERVICE,
      useClass: EventService
    },
    {
      provide: METHOD_SERVICE,
      useClass: MethodService
    },
    {
      provide: APPLICATION_VERSION_SERVICE,
      useClass: ApplicationVersionService
    },
    {
      provide: FEATURE_SENTINEL_SERVICE,
      useClass: FeatureSentinelService
    },
    ApplicationPageGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
