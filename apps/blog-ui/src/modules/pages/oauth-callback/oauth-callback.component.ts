import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {mergeMap, Subscription, take, throwError} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AUTHENTICATION_SERVICE} from '../../../services/authentication-service/authentication.injector';
import {IAuthenticationService} from '../../../services/authentication-service/authentication-service.interface';
import {OauthCallbackQueryParams} from '../../../models/query-params/oauth-callback-query-params';
import {catchError, map} from 'rxjs/operators';
import {ISmartNavigatorService, SMART_NAVIGATOR_SERVICE} from '@ui-tool/core';
import {StorageKeys} from '../../../constants/storage-keys';
import {HomeNavigationRequest} from '../../../models/navigation-requests/home-navigation-request';
import {LocalStorageService} from 'ngx-webstorage';
import {AuthenticationStatuses} from '../../../enumerations/authentication-statuses';

@Component({
  selector: 'oauth-callback',
  templateUrl: 'oauth-callback.component.html',
  styleUrls: ['oauth-callback.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OauthCallbackComponent implements OnInit, OnDestroy {

  //#region Properties

  private __authenticationStatus: AuthenticationStatuses;

  protected readonly _subscription = new Subscription();

  public readonly AuthenticationStatuses = AuthenticationStatuses;

  //#endregion

  //#region Accessors

  public get authenticationStatus(): AuthenticationStatuses {
    return this.__authenticationStatus;
  }

  //#endregion

  //#region Constructor

  public constructor(
    @Inject(AUTHENTICATION_SERVICE)
    protected readonly _authenticationService: IAuthenticationService,
    @Inject(SMART_NAVIGATOR_SERVICE)
    protected readonly _navigationService: ISmartNavigatorService,
    protected readonly _storage: LocalStorageService,
    protected readonly _changeDetectorRef: ChangeDetectorRef,
    protected readonly _activatedRoute: ActivatedRoute) {
    this.__authenticationStatus = AuthenticationStatuses.none;
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const authenticationSubscription = this._activatedRoute.queryParams
      .pipe(
        take(1),
        map(queryParams => queryParams as OauthCallbackQueryParams),
        mergeMap(queryParams => {
          return this._authenticationService.authenticateAsync(queryParams.provider, queryParams.code);
        }),
        mergeMap(authenticationResult => {
          this.__authenticationStatus = AuthenticationStatuses.successful;
          this._storage.store(StorageKeys.authenticationResult, authenticationResult);
          this._changeDetectorRef.markForCheck();
          const navigationRequest = new HomeNavigationRequest();
          return this._navigationService.navigateToScreenAsync(navigationRequest);
        }),
        catchError(exception => {
          this.__authenticationStatus = AuthenticationStatuses.failed;
          this._changeDetectorRef.markForCheck();
          return throwError(exception);
        })
      )
      .subscribe();
    this._subscription.add(authenticationSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  //#endregion
}
