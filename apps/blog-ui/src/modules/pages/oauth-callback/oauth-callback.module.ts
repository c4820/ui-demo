import {NgModule} from '@angular/core';
import {OauthCallbackComponent} from './oauth-callback.component';
import {CommonModule} from '@angular/common';
import {OauthCallbackRoutingModule} from './oauth-callback-routing.module';

@NgModule({
    declarations: [
        OauthCallbackComponent
    ],
  imports: [
    OauthCallbackRoutingModule,
    CommonModule
  ],
    exports: [
        OauthCallbackComponent
    ]
})
export class OauthCallbackModule {
}
