import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import {ScreenCodes} from '../../../constants/screen-codes';
import {Application} from '../../../models/entities/application';
import {Subscription} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ApplicationSelectedChannelEvent
} from '../../../models/channel-events/applications/application-selected.channel-event';
import {ProfileViewModel} from '../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../models/channel-events/profiles/profile-updated.channel-event';

@Component({
  selector: 'application-page',
  templateUrl: 'application-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationPageComponent implements OnInit, OnDestroy {

  //#region Properties

  private __application?: Application;

  private __profile?: ProfileViewModel;

  protected _subscription: Subscription;

  public readonly ScreenCodes = ScreenCodes;

  //#endregion

  //#region Accessors

  @HostBinding('class')
  public get hostClass(): string {
    return 'page';
  }

  public get application(): Application | undefined {
    return this.__application;
  }

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this._subscription = new Subscription();
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {
    const hookApplicationChosenSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationSelectedChannelEvent)
      .subscribe(({application}) => {
        this.__application = application;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookApplicationChosenSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

}
