import {NgModule} from '@angular/core';
import {ApplicationOverviewComponent} from './application-overview.component';
import {ApplicationOverviewRoutingModule} from './application-overview-routing.module';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {SmartNavigatorModule, ValidationSummarizerModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {ApplicationOverviewPageGuard} from '../../../../guards/application-overview-page.guard';
import {MarkdownModule} from 'ngx-markdown';
import {ReactiveFormsModule} from '@angular/forms';
import {WidgetFabModule} from '../../../shared/widgets/widget-fab';
import {DetailedVersionDialogModule} from '../../../shared/dialogs/detailed-version-dialog/detailed-version-dialog.module';

@NgModule({
  imports: [
    RouterModule,
    ApplicationOverviewRoutingModule,
    TranslateModule,
    SmartNavigatorModule,
    CommonModule,
    MarkdownModule.forRoot(),
    ReactiveFormsModule,
    WidgetFabModule,
    DetailedVersionDialogModule,
    ValidationSummarizerModule.forRoot({})
  ],
  declarations: [
    ApplicationOverviewComponent
  ],
  exports: [
    ApplicationOverviewComponent,
  ],
  providers: [
    ApplicationOverviewPageGuard
  ]
})
export class ApplicationOverviewModule {

}
