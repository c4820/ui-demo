import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {
  ISmartNavigatorService,
  ISpinnerService,
  IValidationSummarizerService,
  SMART_NAVIGATOR_SERVICE,
  SPINNER_SERVICE,
  VALIDATION_SUMMARIZER_SERVICE
} from '@ui-tool/core';
import {ScreenCodes} from '../../../../constants/screen-codes';
import {APPLICATION_SERVICE, APPLICATION_VERSION_SERVICE} from '../../../../constants/injectors';
import {finalize, forkJoin, from, mergeMap, Observable, of, Subscription, switchMap} from 'rxjs';
import {IApplicationService} from '../../../../services/interfaces/apis/application-service.interface';
import {Application} from '../../../../models/entities/application';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ApplicationVersionSelectedChannelEvent
} from '../../../../models/channel-events/applications/application-version-selected.channel-event';
import {
  ChangeNavbarTitleChannelEvent
} from '../../../../models/channel-events/navbar/change-navbar-title.channel-event';
import {Title} from '@angular/platform-browser';
import {ApplicationVersion} from '../../../../models/entities/application-version';
import {Router} from '@angular/router';
import {PageViewModes} from '../../../../enumerations/page-view-modes';
import {FormGroup} from '@angular/forms';
import {ApplicationOverviewFields} from './application-overview-fields';
import {SpinnerContainerIds} from '../../../../constants/spinner-container-ids';
import {AddApplicationViewModel} from '../../../../view-models/applications/add-application.view-model';
import {
  ApplicationOverviewNavigationRequest
} from '../../../../models/navigation-requests/application-overview-navigation-request';
import {ToastrService} from 'ngx-toastr';
import {map} from 'rxjs/operators';
import {ApplicationVersionQueryParams} from '../../../../models/query-params/application-version-query-params';
import {HomeNavigationRequest} from '../../../../models/navigation-requests/home-navigation-request';
import {EditApplicationViewModel} from '../../../../view-models/applications/edit-application.view-model';
import {EditableField} from '../../../../models/editable-field';
import {IApplicationVersionService} from '../../../../services/interfaces/apis/application-version-service.interface';
import {ProfileViewModel} from '../../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../../models/channel-events/profiles/profile-updated.channel-event';

@Component({
  selector: 'application-versions-page',
  templateUrl: 'application-overview.component.html',
  styleUrls: ['application-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationOverviewComponent implements OnInit, OnDestroy {

  //#region Properties

  private __viewMode = PageViewModes.READ;

  private __profile?: ProfileViewModel;

  private __application?: Application;

  private __versions: ApplicationVersion[];

  private __versionIdToUrl: Record<string, string>;

  private __addVersionUrl?: string;

  private __fields: ApplicationOverviewFields;

  protected _subscription: Subscription = new Subscription();

  public readonly ScreenCodes = ScreenCodes;

  public readonly PageViewModes = PageViewModes;

  public readonly ControlNames = ApplicationOverviewFields;

  //#endregion

  //#region Accessor

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  public get viewMode(): PageViewModes {
    return this.__viewMode;
  }

  public get application(): Application | undefined {
    return this.__application;
  }

  public get versions(): ApplicationVersion[] {
    return this.__versions;
  }

  public get versionIdToUrl(): Record<string, string> {
    return this.__versionIdToUrl;
  }

  public get addVersionUrl(): string | undefined {
    return this.__addVersionUrl;
  }

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SMART_NAVIGATOR_SERVICE) protected _navigationService: ISmartNavigatorService,
                     @Inject(APPLICATION_SERVICE) protected readonly _applicationService: IApplicationService,
                     @Inject(APPLICATION_VERSION_SERVICE) protected readonly _applicationVersionService: IApplicationVersionService,
                     @Inject(MESSAGE_BUS_SERVICE) protected readonly _messageBusService: IMessageBusService,
                     @Inject(VALIDATION_SUMMARIZER_SERVICE) protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toast: ToastrService,
                     protected readonly _title: Title,
                     protected readonly _router: Router,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__fields = new ApplicationOverviewFields();
    this.__versions = [];
    this.__versionIdToUrl = {};
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {

    this._subscription?.unsubscribe();
    this._subscription = new Subscription();

    const hookApplicationSelectedSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationVersionSelectedChannelEvent)
      .pipe(
        switchMap(({application}) => {
          return forkJoin([of(application), application.id
            ? this._applicationVersionService.getByApplicationIdAsync(application.id)
            : of([])]);
        })
      )
      .subscribe(([application, versions]) => {
        this.__application = application;
        this.__versions = versions;

        if (application.id) {
          this._messageBusService.addMessageInstance(new ChangeNavbarTitleChannelEvent(application.name));
          this._title.setTitle(application.name);
          this.__viewMode = PageViewModes.READ;

          this.__fields.formGroup.get(this.ControlNames.applicationName)?.patchValue(application.name);
          this.__fields.formGroup.get(this.ControlNames.applicationDescription)?.patchValue(application.description);

          this.__addVersionUrl = this._navigationService.buildUrlTree(ScreenCodes.applicationVersion, void (0), {
            queryParams: new ApplicationVersionQueryParams(application.id)
          }).toString();

        } else {
          this.__viewMode = PageViewModes.ADD;
        }

        const versionIdToUrl: Record<string, string> = {};
        for (const version of versions) {
          versionIdToUrl[version.id] = this._navigationService.buildUrlTree(ScreenCodes.applicationVersion, {}, {
            queryParams: new ApplicationVersionQueryParams(application.id, version.id)
          }).toString();
        }

        this.__versionIdToUrl = versionIdToUrl;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookApplicationSelectedSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public goToUrl(url?: string): void {

    if (!url) {
      return;
    }

    const navigationSubscription = from(this._router
      .navigateByUrl(url))
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public submit(): void {

    if (this.__viewMode === PageViewModes.READ) {
      return;
    }

    if (!this.formGroup) {
      return;
    }

    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
    if (this.formGroup.invalid) {
      this._toast.error(`Some fields are invalid. Please check and try again.`);
      return;
    }

    const spinnerId = this._spinnerService
      .displaySpinner(SpinnerContainerIds.mainLayout);

    const nameControl = this.formGroup.get(this.ControlNames.applicationName)!;
    const descriptionControl = this.formGroup.get(this.ControlNames.applicationDescription)!;

    let addEditApplicationObservable: Observable<Application> | null = null;

    if (!this.__application || !this.__application.id) {
      const command = new AddApplicationViewModel(nameControl.value, descriptionControl.value);

      addEditApplicationObservable = this._applicationService.addAsync(command);
    } else {
      const command = new EditApplicationViewModel();
      command.name = new EditableField<string>(nameControl.value, nameControl.dirty || nameControl.touched);
      command.description = new EditableField<string>(descriptionControl.value, descriptionControl.dirty || descriptionControl.touched);

      addEditApplicationObservable = this._applicationService.editAsync(this.__application.id, command);
    }

    if (addEditApplicationObservable == null) {
      return;
    }

    const addEditApplicationSubscription = addEditApplicationObservable
      .pipe(
        mergeMap(application => {
          const navigationRequest = new ApplicationOverviewNavigationRequest(application.id);
          return this._navigationService.navigateToScreenAsync(navigationRequest)
            .pipe(
              map(() => application)
            );
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, spinnerId))
      )
      .subscribe(application => {
        this._toast.success(`Application named ${application.name} has been added successfully.`);
      });
    this._subscription.add(addEditApplicationSubscription);
  }

  public deleteApplication(applicationId: string): void {
    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const deleteApplicationSubscription = this._applicationService
      .deleteByIdAsync(applicationId)
      .pipe(
        mergeMap(() => {
          const navigationRequest = new HomeNavigationRequest();
          return this._navigationService.navigateToScreenAsync(navigationRequest);
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe(() => {
      });
    this._subscription.add(deleteApplicationSubscription);
  }

  public goBack(): void {
    const navigationRequest = new HomeNavigationRequest();
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public toggleEditMode(): void {

    switch (this.__viewMode) {
      case PageViewModes.EDIT:
        this.__viewMode = PageViewModes.READ;
        break;

      default:
        this.__viewMode = PageViewModes.EDIT;
    }
  }

  //#endregion
}
