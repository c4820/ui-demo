import {FormControl, FormGroup, Validators} from '@angular/forms';

export class ApplicationOverviewFields {

  //#region Static properties

  public static readonly applicationName = 'applicationName';

  public static readonly applicationDescription = 'applicationDescription';

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [ApplicationOverviewFields.applicationName]: new FormControl('', [Validators.required]),
      [ApplicationOverviewFields.applicationDescription]: new FormControl('')
    });
  }

//#endregion

  //#region Methods

  //#endregion

}
