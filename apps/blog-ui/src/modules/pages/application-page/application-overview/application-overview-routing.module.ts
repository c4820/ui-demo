import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {ApplicationOverviewComponent} from './application-overview.component';
import {ApplicationOverviewPageGuard} from '../../../../guards/application-overview-page.guard';

//#region Routes

const routes: Route[] = [
  {
    path: '',
    component: ApplicationOverviewComponent,
    canActivate: [ApplicationOverviewPageGuard],
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
  }
];

//#endregion

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ApplicationOverviewRoutingModule {

}
