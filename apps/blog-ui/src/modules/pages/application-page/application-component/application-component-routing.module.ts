import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ApplicationComponentComponent} from './application-component.component';
import {ApplicationComponentsPageGuard} from '../../../../guards/application-components-page.guard';
import {ApplicationComponentPageGuard} from '../../../../guards/application-component-page.guard';
import {ComponentOverviewPageGuard} from '../../../../guards/component-overview-page.guard';

const routes: Routes = [
  {
    path: '',
    component: ApplicationComponentComponent,
    canActivate: [ApplicationComponentsPageGuard],
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
    children: [
      {
        path: 'event',
        canActivate: [ApplicationComponentPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-event/component-event.module')
          .then(m => m.ComponentEventModule)
      },
      {
        path: 'events',
        canActivate: [ApplicationComponentPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-events/component-events.module')
          .then(m => m.ComponentEventsModule)
      },
      {
        path: 'methods',
        canActivate: [ApplicationComponentPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-methods/component-methods.module')
          .then(m => m.ComponentMethodsModule)
      },
      {
        path: 'method',
        canActivate: [ApplicationComponentPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-method/component-method.module')
          .then(m => m.ComponentMethodModule)
      },
      {
        path: 'property',
        canActivate: [ApplicationComponentPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-property/component-property.module')
          .then(m => m.ComponentPropertyModule)
      },
      {
        path: 'properties',
        canActivate: [ApplicationComponentPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-properties/component-properties.module')
          .then(m => m.ComponentPropertiesModule)
      },
      {
        path: 'overview',
        canActivate: [ComponentOverviewPageGuard],
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./component-overview/component-overview.module')
          .then(m => m.ComponentOverviewModule)
      },
      {
        path: '',
        runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
        loadChildren: () => import('./version-overview/version-overview.module')
          .then(m => m.VersionOverviewModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ApplicationComponentRoutingModule {

}
