import {NgModule} from '@angular/core';
import {ComponentEventComponent} from './component-event.component';
import {ComponentEventRoutingModule} from './component-event-routing.module';
import {
  ToDetailedEventTextPipeModule
} from '../../../../../pipes/to-detailed-event-text/to-detailed-event-text-pipe.module';
import {HasAnyValidatorsModule, ToTrustedHtmlPipeModule, ValidationSummarizerModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {ComponentEventPageGuard} from '../../../../../guards/component-event-page.guard';
import {MarkdownModule} from 'ngx-markdown';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    ComponentEventRoutingModule,
    ToDetailedEventTextPipeModule,
    ToTrustedHtmlPipeModule,
    CommonModule,
    WidgetFabModule,
    MarkdownModule.forRoot(),
    ValidationSummarizerModule.forRoot({}),
    ReactiveFormsModule,
    HasAnyValidatorsModule.forRoot()
  ],
  declarations: [
    ComponentEventComponent
  ],
  exports: [
    ComponentEventComponent
  ],
  providers: [
    ComponentEventPageGuard
  ]
})
export class ComponentEventModule {

}
