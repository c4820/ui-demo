import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Property} from '../../../../../models/entities/property';
import {Event as ComponentEvent} from '../../../../../models/entities/event';
import {filter, finalize, mergeMap, Observable, ReplaySubject, Subscription, switchMap, tap} from 'rxjs';
import {Application} from '../../../../../models/entities/application';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {EVENT_SERVICE, PROPERTY_SERVICE} from '../../../../../constants/injectors';
import {IEventService} from '../../../../../services/interfaces/apis/event-service.interface';
import {
  ApplicationComponentEventChosenChannelEvent
} from '../../../../../models/channel-events/components/application-component-event-chosen.channel-event';
import {
  ISmartNavigatorService,
  ISpinnerService,
  IValidationSummarizerService,
  SMART_NAVIGATOR_SERVICE,
  SPINNER_SERVICE,
  VALIDATION_SUMMARIZER_SERVICE
} from '@ui-tool/core';
import {ComponentEventFields} from './component-event-fields';
import {FormGroup} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AddComponentEventViewModel} from '../../../../../view-models/events/add-component-event.view-model';
import {
  ComponentEventNavigationRequest
} from '../../../../../models/navigation-requests/component-event-navigation-request';
import {
  ComponentEventsNavigationRequest
} from '../../../../../models/navigation-requests/component-events-navigation-request';
import {
  ComponentPropertyNavigationRequest
} from '../../../../../models/navigation-requests/component-property-navigation-request';
import {HostKinds} from '../../../../../constants/host-kinds';
import {IPropertyService} from '../../../../../services/interfaces/apis/property-service.interface';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';

@Component({
  selector: 'component-event',
  templateUrl: 'component-event.component.html',
  styleUrls: ['component-event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentEventComponent implements OnInit, OnDestroy {

  //#region Properties

  private __event?: ComponentEvent;

  private __properties: Property[];

  private __application?: Application;

  private __version?: ApplicationVersion;

  private __component?: ComponentEntity;

  private __fields = new ComponentEventFields();

  private __getEventProperties$ = new ReplaySubject<string>(1);

  protected readonly _subscription = new Subscription();

  public readonly ControlNames = ComponentEventFields;

  //#endregion

  //#region Accessors

  public get component(): ComponentEntity | undefined {
    return this.__component;
  }

  public get event(): ComponentEvent | undefined {
    return this.__event;
  }

  public get properties(): Property[] {
    return this.__properties;
  }

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE) protected readonly _messageBusService: IMessageBusService,
                     @Inject(EVENT_SERVICE) protected readonly _eventService: IEventService,
                     @Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly __propertyService: IPropertyService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toast: ToastrService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__properties = [];
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const getPropertiesSubscription = this.__getEventProperties$
      .pipe(
        filter(eventId => eventId != null && eventId.length > 0),
        switchMap(eventId => this.__propertyService.getByEventIdAsync(eventId))
      )
      .subscribe(properties => {
        this.__properties = properties;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getPropertiesSubscription);

    const hookEventChosenSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationComponentEventChosenChannelEvent)
      .subscribe(({application, version, component, event}) => {
        this.__application = application;
        this.__component = component;
        this.__event = event;
        this.__version = version;

        if (event && event.id) {
          this.formGroup.get(this.ControlNames.eventName)?.patchValue(event.name);
          this.formGroup.get(this.ControlNames.description)?.patchValue(event.description);
        }

        this.__getEventProperties$.next(this.__event!.id);
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookEventChosenSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  // Save component event.
  public submit(): void {

    if (!this.formGroup || !this.__component || !this.__component.id
      || !this.__application || !this.__application.id) {
      return;
    }

    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
    if (this.formGroup.invalid) {
      this._toast.error(`Some fields are invalid. Please check and try again.`);
      return;
    }

    const name = this.formGroup.get(this.ControlNames.eventName)!.value;
    const description = this.formGroup.get(this.ControlNames.description)!.value;

    const command = new AddComponentEventViewModel(this.__component!.id, name, description);
    const addEditEventObservable: Observable<ComponentEvent> = this._eventService.addAsync(command)
      .pipe(
        tap(componentEvent => {
          this._toast.success(`Event ${componentEvent.name} has been added successfully.`);
        })
      );

    const addEditEventSubscription = addEditEventObservable
      .pipe(
        mergeMap(componentEvent => {
          const navigationRequest = new ComponentEventNavigationRequest(this.__application!.id,
            this.__version!.id,
            this.__component!.id, componentEvent.id);

          return this._navigationService.navigateToScreenAsync(navigationRequest);
        })
      )
      .subscribe();
    this._subscription.add(addEditEventSubscription);
  }

  // Add a property.
  public addProperty(): void {

    if (!this.__application!.id || !this.__version!.id || !this.__event!.id) {
      return;
    }

    const navigationRequest = new ComponentPropertyNavigationRequest(this.__application!.id, this.__version!.id, this.__component!.id,
      HostKinds.event, this.__event!.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  // Go back to the previous page.
  public back(): void {
    if (!this.__application || !this.__version || !this.__component) {
      return;
    }

    const navigationRequest = new ComponentEventsNavigationRequest(this.__application!.id, this.__version!.id, this.__component!.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public deleteProperty(propertyId: string): void {
    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const deletePropertySubscription = this.__propertyService
      .deleteByIdAsync(propertyId)
      .pipe(
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe(() => {
        this.__getEventProperties$.next(this.__event!.id);
      });
    this._subscription.add(deletePropertySubscription);
  }

  //#endregion
}
