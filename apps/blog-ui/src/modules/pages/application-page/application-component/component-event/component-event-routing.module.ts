import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComponentEventComponent} from './component-event.component';
import {ComponentEventPageGuard} from '../../../../../guards/component-event-page.guard';

const routes: Routes = [
  {
    path: '',
    component: ComponentEventComponent,
    canActivate: [ComponentEventPageGuard],
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentEventRoutingModule {

}
