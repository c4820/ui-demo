import {FormControl, FormGroup, Validators} from '@angular/forms';

export class ComponentEventFields {

  //#region Static properties

  public static readonly eventName = 'EVENT_NAME';

  public static readonly description = 'EVENT_DESCRIPTION';

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [ComponentEventFields.eventName]: new FormControl('', [Validators.required]),
      [ComponentEventFields.description]: new FormControl('', [Validators.required]),
    });
  }

  //#endregion

}
