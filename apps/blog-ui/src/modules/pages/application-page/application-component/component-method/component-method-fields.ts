import {FormControl, FormGroup, Validators} from '@angular/forms';

export class ComponentMethodFields {

  //#region Static properties

  public static readonly methodName = 'NAME';

  public static readonly description = 'DESCRIPTION';

  public static readonly availability = 'AVAILABILITY';

  public static readonly valueType = 'VALUE_TYPE';

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [ComponentMethodFields.methodName]: new FormControl('', [Validators.required]),
      [ComponentMethodFields.description]: new FormControl('', [Validators.required]),
      [ComponentMethodFields.valueType]: new FormControl('', [Validators.required]),
      [ComponentMethodFields.availability]: new FormControl('', [Validators.required])
    });
  }

  //#endregion

}
