import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComponentMethodComponent} from './component-method.component';
import {ComponentMethodPageGuard} from '../../../../../guards/component-method-page.guard';

const routes: Routes = [
  {
    path: '',
    component: ComponentMethodComponent,
    canActivate: [ComponentMethodPageGuard],
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentMethodRoutingModule {

}
