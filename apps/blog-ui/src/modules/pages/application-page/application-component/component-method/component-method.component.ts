import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Property} from '../../../../../models/entities/property';
import {mergeMap, Observable, Subscription, tap} from 'rxjs';
import {Application} from '../../../../../models/entities/application';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {METHOD_SERVICE} from '../../../../../constants/injectors';
import {
  ISmartNavigatorService,
  IValidationSummarizerService,
  SMART_NAVIGATOR_SERVICE,
  VALIDATION_SUMMARIZER_SERVICE
} from '@ui-tool/core';
import {ComponentMethodFields} from './component-method-fields';
import {FormGroup} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {IMethodService} from '../../../../../services/interfaces/apis/method-service.interface';
import {AddMethodViewModel} from '../../../../../view-models/methods/add-method.view-model';
import {
  ComponentMethodNavigationRequest
} from '../../../../../models/navigation-requests/component-method-navigation-request';
import {ItemAvailabilities} from '../../../../../enumerations/item-availabilities';
import {Method} from '../../../../../models/entities/method';
import {
  ComponentMethodChosenChannelEvent
} from '../../../../../models/channel-events/components/component-method-chosen.channel-event';
import {
  ComponentMethodsNavigationRequest
} from '../../../../../models/navigation-requests/component-methods-navigation-request';

@Component({
  selector: 'component-method',
  templateUrl: 'component-method.component.html',
  styleUrls: ['component-method.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentMethodComponent implements OnInit, OnDestroy {

  //#region Properties

  private __method?: Method;

  private __properties: Property[];

  private __application?: Application;

  private __version?: ApplicationVersion;

  private __component?: ComponentEntity;

  private __fields = new ComponentMethodFields();

  protected readonly _subscription = new Subscription();

  public readonly ControlNames = ComponentMethodFields;

  public readonly ItemAvailabilities = ItemAvailabilities;

  //#endregion

  //#region Accessors

  public get component(): ComponentEntity | undefined {
    return this.__component;
  }

  public get method(): Method | undefined {
    return this.__method;
  }

  public get properties(): Property[] {
    return this.__properties;
  }

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE) protected readonly _messageBusService: IMessageBusService,
                     @Inject(METHOD_SERVICE) protected readonly _methodService: IMethodService,
                     @Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     protected readonly _toast: ToastrService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__properties = [];
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const getEventsSubscription = this._messageBusService
      .hookMessageChannelByType(ComponentMethodChosenChannelEvent)
      .subscribe(({application, version, component, method}) => {
        this.__application = application;
        this.__component = component;
        this.__method = method;
        this.__version = version;

        if (method.id) {
          this.formGroup.get(this.ControlNames.methodName)?.patchValue(method.name);
          this.formGroup.get(this.ControlNames.description)?.patchValue(method.description);
          this.formGroup.get(this.ControlNames.valueType)?.patchValue(method.valueType);
          this.formGroup.get(this.ControlNames.availability)?.patchValue(method.availability);
        }
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getEventsSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  // Save component event.
  public submit(): void {

    if (!this.formGroup || !this.__component || !this.__component.id
      || !this.__application || !this.__application.id) {
      return;
    }

    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
    if (this.formGroup.invalid) {
      this._toast.error(`Some fields are invalid. Please check and try again.`);
      return;
    }

    const name = this.formGroup.get(this.ControlNames.methodName)!.value;
    const description = this.formGroup.get(this.ControlNames.description)!.value;
    const valueType = this.formGroup.get(this.ControlNames.valueType)!.value;
    const availability = this.formGroup.get(this.ControlNames.availability)!.value;

    const command = new AddMethodViewModel(this.__component.id, name, description, valueType, availability);
    const addEditMethodObservable: Observable<Method> = this._methodService.addAsync(command)
      .pipe(
        tap(method => {
          this._toast.success(`Method ${method.name} has been added successfully.`);
        })
      );

    const addEditMethodSubscription = addEditMethodObservable
      .pipe(
        mergeMap(method => {
          const navigationRequest = new ComponentMethodNavigationRequest(this.__application!.id,
            this.__version!.id,
            this.__component!.id, method.id);

          return this._navigationService.navigateToScreenAsync(navigationRequest);
        })
      )
      .subscribe();
    this._subscription.add(addEditMethodSubscription);
  }

  // Add a property.
  public addProperty(): void {

  }

  // Go back to the previous page.
  public back(): void {

    if (!this.__application || !this.__version || !this.__component) {
      return;
    }

    const navigationRequest = new ComponentMethodsNavigationRequest(this.__application!.id, this.__version!.id, this.__component!.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  //#endregion
}
