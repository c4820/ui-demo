import {NgModule} from '@angular/core';
import {ComponentMethodComponent} from './component-method.component';
import {ComponentMethodRoutingModule} from './component-method-routing.module';
import {
  ToDetailedEventTextPipeModule
} from '../../../../../pipes/to-detailed-event-text/to-detailed-event-text-pipe.module';
import {HasAnyValidatorsModule, ToTrustedHtmlPipeModule, ValidationSummarizerModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {MarkdownModule} from 'ngx-markdown';
import {ReactiveFormsModule} from '@angular/forms';
import {ComponentMethodPageGuard} from '../../../../../guards/component-method-page.guard';

@NgModule({
  imports: [
    ComponentMethodRoutingModule,
    ToDetailedEventTextPipeModule,
    ToTrustedHtmlPipeModule,
    CommonModule,
    WidgetFabModule,
    MarkdownModule.forRoot(),
    ValidationSummarizerModule.forRoot({}),
    ReactiveFormsModule,
    HasAnyValidatorsModule.forRoot()
  ],
  declarations: [
    ComponentMethodComponent
  ],
  exports: [
    ComponentMethodComponent
  ],
  providers: [
    ComponentMethodPageGuard
  ]
})
export class ComponentMethodModule {

}
