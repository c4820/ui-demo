import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {filter, forkJoin, from, of, Subject, Subscription, switchMap} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {COMPONENT_SERVICE, EVENT_SERVICE, METHOD_SERVICE, PROPERTY_SERVICE} from '../../../../constants/injectors';
import {IMethodService} from '../../../../services/interfaces/apis/method-service.interface';
import {IPropertyService} from '../../../../services/interfaces/apis/property-service.interface';
import {IEventService} from '../../../../services/interfaces/apis/event-service.interface';
import {Title} from '@angular/platform-browser';
import {Component as ComponentEntity} from '../../../../models/entities/component';
import {ScreenCodes} from '../../../../constants/screen-codes';
import {Application} from '../../../../models/entities/application';
import {ActivatedRoute, Router} from '@angular/router';
import {ApplicationVersion} from '../../../../models/entities/application-version';
import {IComponentService} from '../../../../services/interfaces/apis/component-service.interface';
import {ISmartNavigatorService, SMART_NAVIGATOR_SERVICE} from '@ui-tool/core';
import {ComponentOverviewPageQueryParams} from '../../../../models/query-params/component-overview-page-query-params';
import {
  ChangeNavbarTitleChannelEvent
} from '../../../../models/channel-events/navbar/change-navbar-title.channel-event';
import {ApplicationOverviewQueryParams} from '../../../../models/query-params/application-overview-query-params';
import {
  ComponentPropertiesPageQueryParams
} from '../../../../models/query-params/component-properties-page-query-params';
import {ComponentEventsPageQueryParams} from '../../../../models/query-params/component-events-page-query-params';
import {ComponentMethodsPageQueryParams} from '../../../../models/query-params/component-methods-page-query-params';
import {
  ApplicationVersionSelectedChannelEvent
} from '../../../../models/channel-events/applications/application-version-selected.channel-event';
import {ProfileViewModel} from '../../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../../models/channel-events/profiles/profile-updated.channel-event';
import {ApplicationVersionQueryParams} from '../../../../models/query-params/application-version-query-params';

@Component({
  selector: 'application-component',
  templateUrl: 'application-component.component.html',
  styleUrls: ['application-component.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationComponentComponent implements OnInit, OnDestroy {

  //#region Properties

  private __profile?: ProfileViewModel;

  private __components: ComponentEntity[];

  private __componentIdToOverviewUrl: Record<string, string>;

  private __componentIdToPropertyUrl: Record<string, string>;

  private __componentIdToEventUrl: Record<string, string>;

  private __componentToMethodUrl: Record<string, string>;

  private __componentIdToMethodUrl: Record<string, string>;

  private __applicationOverviewUrl: string;

  private __addComponentUrl: string | undefined;

  private __versionOverviewUrl: string | undefined;

  private __application?: Application;

  private __version?: ApplicationVersion;

  private __getComponentParts$ = new Subject<void>();

  protected _subscription: Subscription;

  //#endregion

  //#region Accessors

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  public get version(): ApplicationVersion | undefined {
    return this.__version;
  }

  public get components(): ComponentEntity[] {
    return this.__components;
  }

  public get componentIdToOverviewUrl(): Record<string, string> {
    return this.__componentIdToOverviewUrl;
  }

  public get componentIdToPropertyUrl(): Record<string, string> {
    return this.__componentIdToPropertyUrl;
  }

  public get componentIdToEventUrl(): Record<string, string> {
    return this.__componentIdToEventUrl;
  }

  public get componentToMethodUrl(): Record<string, string> {
    return this.__componentToMethodUrl;
  }

  public get componentIdToMethodUrl(): Record<string, string> {
    return this.__componentIdToMethodUrl;
  }

  public get versionOverviewUrl(): string | undefined {
    return this.__versionOverviewUrl;
  }

  public get applicationOverviewUrl(): string {
    return this.__applicationOverviewUrl;
  }

  public get addComponentUrl(): string | undefined {
    return this.__addComponentUrl;
  }

  public get hasAnyComponent(): boolean {
    return this.__components && this.__components.length > 0;
  }

  public get hasVersion(): boolean {
    return this.__version != null && this.__version.id.length > 0;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(METHOD_SERVICE)
                     protected readonly _methodService: IMethodService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService,
                     @Inject(EVENT_SERVICE)
                     protected readonly _eventService: IEventService,
                     @Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     protected readonly _activatedRoute: ActivatedRoute,
                     protected readonly _title: Title,
                     protected readonly _changeDetectorRef: ChangeDetectorRef,
                     protected readonly _router: Router) {

    this.__components = [];
    this.__componentIdToOverviewUrl = {};
    this.__componentIdToPropertyUrl = {};
    this.__componentIdToEventUrl = {};
    this.__componentToMethodUrl = {};
    this.__componentIdToMethodUrl = {};
    this.__applicationOverviewUrl = '';
    this._subscription = new Subscription();
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    this._subscription?.unsubscribe();
    this._subscription = new Subscription();

    const queryParamsChangedSubscription = this._activatedRoute
      .queryParams
      .subscribe(() => {
        this.__getComponentParts$.next();
      });
    this._subscription.add(queryParamsChangedSubscription);

    const selectApplicationComponentSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationVersionSelectedChannelEvent)
      .pipe(
        filter(({application, version}) => {
          return application != null && version != null;
        }),
        switchMap(({application, version}) => {
          return forkJoin([of(application), of(version),
            this._componentService.getByApplicationIdAsync(application.id, version!.id)]);
        })
      )
      .subscribe(([application, version, components]) => {

        const componentIdToPropertyUrl: Record<string, string> = {};
        const componentIdToEventUrl: Record<string, string> = {};
        const componentIdToMethodUrl: Record<string, string> = {};
        const componentIdToOverviewUrl: Record<string, string> = {};

        for (const component of components || []) {

          componentIdToOverviewUrl[component.id] = this._navigationService
            .buildUrlTree(ScreenCodes.componentOverview, {}, {
              queryParams: new ComponentOverviewPageQueryParams(application.id, version!.id, component.id)
            })
            .toString();

          componentIdToPropertyUrl[component.id] = this._navigationService
            .buildUrlTree(ScreenCodes.componentProperties, {}, {
              queryParams: new ComponentPropertiesPageQueryParams(application.id, version!.id, component.id)
            })
            .toString();

          componentIdToEventUrl[component.id] = this._navigationService
            .buildUrlTree(ScreenCodes.componentEvents, {}, {
              queryParams: new ComponentEventsPageQueryParams(application.id, version!.id, component.id)
            })
            .toString();

          componentIdToMethodUrl[component.id] = this._navigationService
            .buildUrlTree(ScreenCodes.componentMethods, {}, {
              queryParams: new ComponentMethodsPageQueryParams(application.id, version!.id, component.id)
            })
            .toString();
        }

        this.__components = components;
        this.__application = application;
        this.__version = version;
        this.__componentIdToPropertyUrl = componentIdToPropertyUrl;
        this.__componentIdToEventUrl = componentIdToEventUrl;
        this.__componentIdToMethodUrl = componentIdToMethodUrl;
        this.__componentIdToOverviewUrl = componentIdToOverviewUrl;

        this.__applicationOverviewUrl = encodeURI(this._navigationService
          .buildUrlTree(ScreenCodes.applicationOverview, void (0), {
            queryParams: new ApplicationOverviewQueryParams(application.id)
          })
          .toString());

        this.__addComponentUrl = encodeURI(this._navigationService
          .buildUrlTree(ScreenCodes.componentOverview, void (0), {
            queryParams: new ComponentOverviewPageQueryParams(application.id, version!.id)
          })
          .toString());

        this.__versionOverviewUrl = this._navigationService.buildUrlTree(ScreenCodes.applicationVersion, void (0), {
          queryParams: new ApplicationVersionQueryParams(application.id, version!.id)
        }).toString();

        this._messageBusService.addMessageInstance(new ChangeNavbarTitleChannelEvent(application.name));
        this.__getComponentParts$.next();
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(selectApplicationComponentSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public goToUrl(url: string | undefined): void {
    if (!url) {
      return;
    }

    const navigationSubscription = from(this._router
      .navigateByUrl(url))
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  //#endregion
}
