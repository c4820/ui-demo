import {NgModule} from '@angular/core';
import {ComponentMethodsComponent} from './component-methods.component';
import {ComponentMethodsRoutingModule} from './component-methods-routing.module';
import {CommonModule} from '@angular/common';
import {
  ToDetailedMethodTextPipeModule
} from '../../../../../pipes/to-detailed-method-text/to-detailed-method-text-pipe.module';
import {ToTrustedHtmlPipeModule} from '@ui-tool/core';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';
import {MarkdownModule} from 'ngx-markdown';

@NgModule({
  imports: [
    ComponentMethodsRoutingModule,
    CommonModule,
    ToDetailedMethodTextPipeModule,
    ToTrustedHtmlPipeModule,
    WidgetFabModule,
    NgbAccordionModule,
    MarkdownModule.forRoot()
  ],
  declarations: [
    ComponentMethodsComponent
  ],
  exports: [
    ComponentMethodsComponent
  ]
})
export class ComponentMethodsModule {

}
