import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComponentMethodsComponent} from './component-methods.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentMethodsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentMethodsRoutingModule {

}
