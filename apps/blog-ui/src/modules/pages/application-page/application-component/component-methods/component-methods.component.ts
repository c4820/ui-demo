import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Method} from '../../../../../models/entities/method';
import {Property} from '../../../../../models/entities/property';
import {filter, finalize, ReplaySubject, Subject, Subscription, switchMap, throwError} from 'rxjs';
import {Application} from '../../../../../models/entities/application';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {
  ApplicationComponentChosenChannelEvent
} from '../../../../../models/channel-events/components/application-component-chosen.channel-event';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {METHOD_SERVICE} from '../../../../../constants/injectors';
import {IMethodService} from '../../../../../services/interfaces/apis/method-service.interface';
import {
  ComponentMethodNavigationRequest
} from '../../../../../models/navigation-requests/component-method-navigation-request';
import {ISmartNavigatorService, ISpinnerService, SMART_NAVIGATOR_SERVICE, SPINNER_SERVICE} from '@ui-tool/core';
import {Router} from '@angular/router';
import {ScreenCodes} from '../../../../../constants/screen-codes';
import {ComponentMethodPageQueryParams} from '../../../../../models/query-params/component-method-page-query-params';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';
import {ToastrService} from 'ngx-toastr';
import {catchError} from 'rxjs/operators';
import {ProfileViewModel} from '../../../../../view-models/profiles/profile.view-model';
import {
  ApplicationOverviewNavigationRequest
} from '../../../../../models/navigation-requests/application-overview-navigation-request';

@Component({
  selector: 'component-methods',
  templateUrl: 'component-methods.component.html',
  styleUrls: ['component-methods.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentMethodsComponent implements OnInit, OnDestroy {

  //#region Properties

  private __profile?: ProfileViewModel;

  private __application?: Application;

  private __version?: ApplicationVersion;

  private __component?: ComponentEntity;

  private __methods: Method[];

  private __methodIdToEditUrl: Record<string, string>;

  private __methodToProperties: Record<string, Property[]>;

  private __getMethods$: Subject<void>;

  protected readonly _subscription = new Subscription();

  //#endregion

  //#region Accessors

  public get application(): Application | undefined {
    return this.__application;
  }

  public get version(): ApplicationVersion | undefined {
    return this.__version;
  }

  public get component(): ComponentEntity | undefined {
    return this.__component;
  }

  public get methods(): Method[] {
    return this.__methods;
  }

  public get methodToProperties(): Record<string, Property[]> {
    return this.__methodToProperties;
  }

  public get methodIdToEditUrl(): Record<string, string> {
    return this.__methodIdToEditUrl;
  }

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(METHOD_SERVICE)
                     protected readonly _methodService: IMethodService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toast: ToastrService,
                     protected readonly _router: Router,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__methods = [];
    this.__methodToProperties = {};
    this.__methodIdToEditUrl = {};

    this.__getMethods$ = new ReplaySubject(1);
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const getMethodsSubscription = this.__getMethods$
      .pipe(
        filter(() => this.__component?.id !== null &&
          this.__component?.id !== undefined &&
          this.__component.id.length > 0),
        switchMap(() => {
          const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
          return this._methodService.getByComponentIdAsync(this.__component!.id)
            .pipe(
              finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
            );
        })
      )
      .subscribe(methods => {
        const methodIdToEditUrl: Record<string, string> = {};
        for (const method of methods) {
          methodIdToEditUrl[method.id] = this._navigationService
            .buildUrlTree(ScreenCodes.componentMethod, void (0), {
              queryParams: new ComponentMethodPageQueryParams(this.application!.id, this.version!.id, this.component!.id, method.id)
            })
            .toString();
        }
        this.__methodIdToEditUrl = methodIdToEditUrl;
        this.__methods = methods;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getMethodsSubscription);

    const hookChosenComponentSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .subscribe(({application, version, component}) => {
        this.__application = application;
        this.__version = version;
        this.__component = component;
        this._changeDetectorRef.markForCheck();

        this.__getMethods$.next();
      });
    this._subscription.add(hookChosenComponentSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public goToAddMethodPage(): void {

    if (!this.__application || !this.__application!.id || !this.__version!.id) {
      return;
    }

    const navigationRequest = new ComponentMethodNavigationRequest(this.__application.id, this.__version!.id, this.__component!.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public goToUrl(url: string | undefined): void {
    if (!url) {
      return;
    }

    void this._router.navigateByUrl(url);
  }

  public deleteMethod(methodId: string): void {

    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const deleteMethodSubscription = this._methodService
      .deleteByIdAsync(methodId)
      .pipe(
        catchError(error => {
          this._toast.error('Failed to delete the method');
          return throwError(error);
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe(() => {
        this._toast.success('Method has been deleted successfully');
        this.__getMethods$.next();
      });
    this._subscription.add(deleteMethodSubscription);
  }

  // To go back to the previous page.
  public goBack(): void {

    if (!this.__application || !this.__application.id) {
      return;
    }

    const navigationRequest = new ApplicationOverviewNavigationRequest(this.__application?.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }


  //#endregion
}
