import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {Property} from '../../../../../models/entities/property';
import {
  ApplicationComponentChosenChannelEvent
} from '../../../../../models/channel-events/components/application-component-chosen.channel-event';
import {Application} from '../../../../../models/entities/application';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {filter, finalize, forkJoin, from, of, ReplaySubject, Subject, Subscription, switchMap, throwError} from 'rxjs';
import {IApplicationService} from '../../../../../services/interfaces/apis/application-service.interface';
import {APPLICATION_SERVICE, COMPONENT_SERVICE, PROPERTY_SERVICE} from '../../../../../constants/injectors';
import {IComponentService} from '../../../../../services/interfaces/apis/component-service.interface';
import {IPropertyService} from '../../../../../services/interfaces/apis/property-service.interface';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {ISmartNavigatorService, ISpinnerService, SMART_NAVIGATOR_SERVICE, SPINNER_SERVICE} from '@ui-tool/core';
import {
  ComponentPropertyNavigationRequest
} from '../../../../../models/navigation-requests/component-property-navigation-request';
import {HostKinds} from '../../../../../constants/host-kinds';
import {Router} from '@angular/router';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {ProfileViewModel} from '../../../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../../../models/channel-events/profiles/profile-updated.channel-event';
import {
  ApplicationOverviewNavigationRequest
} from '../../../../../models/navigation-requests/application-overview-navigation-request';

@Component({
  selector: 'component-properties',
  templateUrl: 'component-properties.component.html',
  styleUrls: ['component-properties.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentPropertiesComponent implements OnInit, OnDestroy {

  //#region Properties

  private __profile?: ProfileViewModel;

  private __application?: Application;

  private __applicationVersion?: ApplicationVersion;

  private __component?: ComponentEntity;

  private __properties: Property[];

  private __propertyIdToEditUrl: Record<string, string>;

  private __getProperties$: Subject<void>;

  protected readonly _subscription: Subscription;

  //#endregion

  //#region Accessors

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  public get application(): Application | undefined {
    return this.__application;
  }

  public get propertyIdToEditUrl(): Record<string, string> {
    return this.__propertyIdToEditUrl;
  }

  public get component(): ComponentEntity | undefined {
    return this.__component;
  }

  public get properties(): Property[] {
    return this.__properties;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(APPLICATION_SERVICE)
                     protected readonly _applicationService: IApplicationService,
                     @Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toast: ToastrService,
                     protected readonly _router: Router,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__properties = [];
    this.__propertyIdToEditUrl = {};
    this.__getProperties$ = new ReplaySubject(1);
    this._subscription = new Subscription();
  }

  //#endregion

  //#region Methods

  public ngOnInit(): void {

    const getPropertiesSubscription = this.__getProperties$
      .pipe(
        filter(() => this.__component?.id != null && this.__component.id.length > 0),
        switchMap(() => {
          const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
          return this._propertyService.getByComponentIdAsync(this.__component!.id)
            .pipe(
              finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
            );
        })
      )
      .subscribe(properties => {
        this.__properties = properties;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getPropertiesSubscription);

    const hookComponentChosenSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .pipe(
        switchMap(({application, version, component}) => {
          return forkJoin([
            of(application), of(version), of(component)]);
        })
      )
      .subscribe(([application, version, component]) => {
        this.__application = application;
        this.__applicationVersion = version;
        this.__component = component;
        this._changeDetectorRef.markForCheck();

        this.__getProperties$.next();
      });
    this._subscription.add(hookComponentChosenSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public deleteProperty(propertyId: string): void {
    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const deleteMethodSubscription = this._propertyService
      .deleteByIdAsync(propertyId)
      .pipe(
        catchError(error => {
          this._toast.error('Failed to delete the property');
          return throwError(error);
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe(() => {
        this._toast.success('Property has been deleted successfully');
        this.__getProperties$.next();
      });
    this._subscription.add(deleteMethodSubscription);

  }

  public goToAddPropertyPage(): void {

    if (!this.__application || !this.__applicationVersion || !this.__component) {
      return;
    }

    const navigationRequest = new ComponentPropertyNavigationRequest(this.__application!.id,
      this.__applicationVersion!.id,
      this.__component!.id, HostKinds.component, this.__component!.id);

    const navigationSubscription = this._navigationService
      .navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public goToUrl(url?: string): void {

    if (!url) {
      return;
    }

    const navigationSubscription = from(this._router
      .navigateByUrl(url))
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  // To go back to the previous page.
  public goBack(): void {

    if (!this.__application || !this.__application.id) {
      return;
    }

    const navigationRequest = new ApplicationOverviewNavigationRequest(this.__application?.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  //#endregion

}
