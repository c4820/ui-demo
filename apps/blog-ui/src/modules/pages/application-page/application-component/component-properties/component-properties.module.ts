import {NgModule} from '@angular/core';
import {ComponentPropertiesComponent} from './component-properties.component';
import {CommonModule} from '@angular/common';
import {ComponentPropertiesRoutingModule} from './component-properties-routing.module';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {MarkdownModule} from 'ngx-markdown';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    ComponentPropertiesComponent
  ],
  imports: [
    CommonModule,
    ComponentPropertiesRoutingModule,
    WidgetFabModule,
    MarkdownModule.forRoot(),
    NgbAccordionModule
  ],
  exports: [
    ComponentPropertiesComponent
  ]
})
export class ComponentPropertiesModule {
}
