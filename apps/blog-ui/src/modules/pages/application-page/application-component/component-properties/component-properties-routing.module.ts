import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComponentPropertiesComponent} from './component-properties.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentPropertiesComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentPropertiesRoutingModule {

}
