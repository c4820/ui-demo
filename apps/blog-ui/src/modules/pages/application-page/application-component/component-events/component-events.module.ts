import {NgModule} from '@angular/core';
import {ComponentEventsComponent} from './component-events.component';
import {ComponentEventsRoutingModule} from './component-events-routing.module';
import {
  ToDetailedEventTextPipeModule
} from '../../../../../pipes/to-detailed-event-text/to-detailed-event-text-pipe.module';
import {SmartNavigatorModule, ToTrustedHtmlPipeModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {RouterModule} from '@angular/router';
import {MarkdownModule} from 'ngx-markdown';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    ComponentEventsRoutingModule,
    ToDetailedEventTextPipeModule,
    ToTrustedHtmlPipeModule,
    CommonModule,
    WidgetFabModule,
    RouterModule,
    SmartNavigatorModule,
    MarkdownModule.forRoot(),
    NgbAccordionModule
  ],
  declarations: [
    ComponentEventsComponent
  ],
  exports: [
    ComponentEventsComponent
  ]
})
export class ComponentEventsModule {

}
