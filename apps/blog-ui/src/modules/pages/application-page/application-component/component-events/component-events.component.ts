import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Property} from '../../../../../models/entities/property';
import {Event} from '../../../../../models/entities/event';
import {filter, finalize, ReplaySubject, Subject, Subscription, switchMap, throwError} from 'rxjs';
import {Application} from '../../../../../models/entities/application';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {
  ApplicationComponentChosenChannelEvent
} from '../../../../../models/channel-events/components/application-component-chosen.channel-event';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {EVENT_SERVICE, PROPERTY_SERVICE} from '../../../../../constants/injectors';
import {IEventService} from '../../../../../services/interfaces/apis/event-service.interface';
import {ISmartNavigatorService, ISpinnerService, SMART_NAVIGATOR_SERVICE, SPINNER_SERVICE} from '@ui-tool/core';
import {Router} from '@angular/router';
import {
  ComponentEventNavigationRequest
} from '../../../../../models/navigation-requests/component-event-navigation-request';
import {ScreenCodes} from '../../../../../constants/screen-codes';
import {ComponentEventPageQueryParams} from '../../../../../models/query-params/component-event-page-query-params';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {IPropertyService} from '../../../../../services/interfaces/apis/property-service.interface';
import {ProfileViewModel} from '../../../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../../../models/channel-events/profiles/profile-updated.channel-event';
import {
  ApplicationOverviewNavigationRequest
} from '../../../../../models/navigation-requests/application-overview-navigation-request';

@Component({
  selector: 'component-events',
  templateUrl: 'component-events.component.html',
  styleUrls: ['component-events.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentEventsComponent implements OnInit, OnDestroy {

  //#region Properties

  private __profile?: ProfileViewModel;

  private __application?: Application;

  private __version?: ApplicationVersion;

  private __events: Event[];

  private __component?: ComponentEntity;

  private __eventIdToProperties: Record<string, Property[]>;

  private __eventIdToEditUrl: Record<string, string>;

  private __getEvents$: Subject<void>;

  private __getEventProperties$: Subject<string[]>;

  protected readonly _subscription = new Subscription();

  //#endregion

  //#region Accessors

  public get component(): ComponentEntity | undefined {
    return this.__component;
  }

  public get application(): Application | undefined {
    return this.__application;
  }

  public get events(): Event[] {
    return this.__events;
  }

  public get eventIdToProperties(): Record<string, Property[]> {
    return this.__eventIdToProperties;
  }

  public get eventIdToEditUrl(): Record<string, string> {
    return this.__eventIdToEditUrl;
  }

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(MESSAGE_BUS_SERVICE) protected readonly _messageBusService: IMessageBusService,
                     @Inject(EVENT_SERVICE)
                     protected readonly _eventService: IEventService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService,
                     protected readonly _toast: ToastrService,
                     protected readonly _router: Router,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__events = [];
    this.__eventIdToProperties = {};
    this.__eventIdToEditUrl = {};

    this.__getEvents$ = new ReplaySubject(1);
    this.__getEventProperties$ = new ReplaySubject(1);
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const getEventsSubscription = this.__getEvents$
      .pipe(
        filter(() => this.__application?.id != null && this.__version?.id != null && this.__component?.id != null),
        switchMap(() => {
          const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
          return this._eventService.getByComponentIdAsync(this.component!.id)
            .pipe(
              finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
            );
        })
      )
      .subscribe(events => {
        const eventIdToEditUrl: Record<string, string> = {};
        for (const event of events) {
          eventIdToEditUrl[event.id] = encodeURI(this._navigationService
            .buildUrlTree(ScreenCodes.componentEvent, void (0), {
              queryParams: new ComponentEventPageQueryParams(this.__application!.id, this.__version!.id, this.__component!.id, event.id)
            })
            .toString());
        }

        const eventIds = events.map(x => x.id);
        this.__getEventProperties$.next(eventIds);

        this.__events = events;

        this.__eventIdToEditUrl = eventIdToEditUrl;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getEventsSubscription);

    const getPropertiesSubscription = this.__getEventProperties$
      .pipe(
        filter(eventIds => eventIds != null && eventIds.length > 0),
        switchMap(eventIds => {
          return this._propertyService.getEventIdToPropertiesAsync(eventIds);
        })
      )
      .subscribe(eventIdToProperties => {
        this.__eventIdToProperties = eventIdToProperties;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getPropertiesSubscription);

    const hookComponentChosenSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .subscribe(({application, version, component}) => {
        this.__application = application;
        this.__version = version;
        this.__component = component;
        this._changeDetectorRef.markForCheck();

        this.__getEvents$.next();
      });
    this._subscription.add(hookComponentChosenSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public goToAddEventPage(): void {

    if (!this.__application || !this.__version || !this.__component) {
      return;
    }
    const navigationRequest = new ComponentEventNavigationRequest(this.__application!.id, this.__version!.id, this.__component!.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public goToUrl(url: string): void {
    void this._router.navigateByUrl(url);
  }

  public deleteEvent(eventId: string): void {

    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const deleteEventSubscription = this._eventService
      .deleteByIdAsync(eventId)
      .pipe(
        catchError(error => {
          this._toast.error('Failed to delete the event');
          return throwError(error);
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe(() => {
        this._toast.success('Event has been deleted successfully');
        this.__getEvents$.next();
      });
    this._subscription.add(deleteEventSubscription);
  }

  // To go back to the previous page.
  public goBack(): void {

    if (!this.__application || !this.__application.id) {
      return;
    }

    const navigationRequest = new ApplicationOverviewNavigationRequest(this.__application?.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }


  //#endregion
}
