import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComponentEventsComponent} from './component-events.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentEventsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentEventsRoutingModule {

}
