import {FormControl, FormGroup, Validators} from '@angular/forms';

export class ComponentOverviewFields {

  //#region Static properties

  public static readonly componentName = 'COMPONENT_NAME';

  public static readonly kind = 'COMPONENT_KIND';

  public static readonly description = 'COMPONENT_DESCRIPTION';

  public static readonly availability = 'COMPONENT_AVAILABILITY';

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Properties

  private readonly __formGroup;

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [ComponentOverviewFields.componentName]: new FormControl('', [
        Validators.required
      ]),
      [ComponentOverviewFields.kind]: new FormControl('', [
        Validators.required
      ]),
      [ComponentOverviewFields.description]: new FormControl('', [
        Validators.required
      ]),
      [ComponentOverviewFields.availability]: new FormControl('', [
        Validators.required
      ])
    });
  }

  //#endregion

}
