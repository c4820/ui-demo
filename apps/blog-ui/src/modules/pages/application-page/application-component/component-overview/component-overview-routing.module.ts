import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ComponentOverviewComponent} from './component-overview.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentOverviewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentOverviewRoutingModule {
}
