import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import {ComponentOverviewFields} from './component-overview-fields';
import {FormGroup} from '@angular/forms';
import {
  ISmartNavigatorService, ISpinnerService,
  IValidationSummarizerService,
  SMART_NAVIGATOR_SERVICE, SPINNER_SERVICE,
  VALIDATION_SUMMARIZER_SERVICE
} from '@ui-tool/core';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ApplicationComponentChosenChannelEvent
} from '../../../../../models/channel-events/components/application-component-chosen.channel-event';
import {Application} from '../../../../../models/entities/application';
import {
  finalize,
  mergeMap,
  Observable,
  of,
  ReplaySubject,
  Subject,
  Subscription,
  switchMap,
  tap,
  throwError
} from 'rxjs';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {
  ApplicationVersionNavigationRequest
} from '../../../../../models/navigation-requests/application-version-navigation-request';
import {ToastrService} from 'ngx-toastr';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {COMPONENT_SERVICE} from '../../../../../constants/injectors';
import {IComponentService} from '../../../../../services/interfaces/apis/component-service.interface';
import {AddComponentViewModel} from '../../../../../view-models/components/add-component.view-model';
import {ItemAvailabilities} from '../../../../../enumerations/item-availabilities';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';
import {catchError} from 'rxjs/operators';
import {
  ComponentOverviewNavigationRequest
} from '../../../../../models/navigation-requests/component-overview-navigation-request';
import {ProfileViewModel} from '../../../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../../../models/channel-events/profiles/profile-updated.channel-event';

@Component({
  selector: 'component-overview',
  templateUrl: 'component-overview.component.html',
  styleUrls: ['component-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentOverviewComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region Properties

  private __application?: Application;

  private __profile?: ProfileViewModel;

  private __component?: ComponentEntity;

  private __version?: ApplicationVersion;

  private __availableKinds: string[] = [];

  private __getAvailableKinds$: Subject<void>;

  private readonly __fields = new ComponentOverviewFields();

  protected readonly _subscription = new Subscription();

  public readonly ControlNames = ComponentOverviewFields;

  public readonly ItemAvailabilities = ItemAvailabilities;

  //#endregion

  //#region Accessors

  public get application(): Application | undefined {
    return this.__application;
  }

  public get component(): ComponentEntity | undefined {
    return this.__component;
  }

  public get version(): ApplicationVersion | undefined {
    return this.__version;
  }

  public get availableKinds(): string[] {
    return this.__availableKinds;
  }

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     @Inject(COMPONENT_SERVICE)
                     protected readonly _componentService: IComponentService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinerService: ISpinnerService,
                     protected readonly _toast: ToastrService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__getAvailableKinds$ = new ReplaySubject<void>(1);
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const hookComponentChosenSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationComponentChosenChannelEvent)
      .subscribe(({application, component, version}) => {
        this.__application = application;
        this.__component = component;
        this.__version = version;

        this.formGroup.get(this.ControlNames.componentName)!
          .patchValue(component.name);
        this.formGroup.get(this.ControlNames.kind)!.patchValue(component.kind);
        this.formGroup.get(this.ControlNames.availability)!.patchValue(component.availability);
        this.formGroup.get(this.ControlNames.description)!.patchValue(component.description);

        this.__getAvailableKinds$.next();
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookComponentChosenSubscription);

    const getAvailableKindSubscription = this.__getAvailableKinds$
      .pipe(
        switchMap(() => {
          const displaySpinnerRequestId = this._spinerService.displaySpinner(SpinnerContainerIds.mainLayout);
          return this._componentService.getAvailableKindsAsync()
            .pipe(
              finalize(() => this._spinerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
            );
        })
      )
      .subscribe(availableKinds => {
        this.__availableKinds = availableKinds;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(getAvailableKindSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;

        if (!profile) {
          this.formGroup.disable();
        }

        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngAfterViewInit(): void {
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public goBack(): void {

    if (!this.__application || !this.__application.id
      || !this.__version || !this.__version.id) {
      return;
    }

    const navigationRequest = new ApplicationVersionNavigationRequest(this.__application.id, this.__version.id);
    const navigationSubscription = this._navigationService
      .navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public submit(): void {

    if (!this.formGroup) {
      return;
    }

    if (!this.__application || !this.__version || !this.__component) {
      return;
    }

    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
    if (this.formGroup.invalid) {
      this._toast.error(`Some fields are invalid. Please check and try again.`);
      return;
    }

    const displaySpinnerRequestId = this._spinerService.displaySpinner(SpinnerContainerIds.mainLayout);
    let addEditComponentObservable: Observable<ComponentEntity>;
    if (!this.__component!.id) {
      const addComponentCommand = new AddComponentViewModel(
        this.__application.id,
        this.__version.id,
        this.formGroup.get(this.ControlNames.componentName)!.value,
        this.formGroup.get(this.ControlNames.kind)!.value,
        this.formGroup.get(this.ControlNames.description)!.value,
        this.formGroup.get(this.ControlNames.availability)!.value);
      addEditComponentObservable = this._componentService.addAsync(addComponentCommand)
        .pipe(
          tap(addedComponent => {
            this._toast.success(`Component ${addedComponent.name} has been added successfully.`);
          }),
          catchError(error => {
            this._toast.success(`Failed to add a new component.`);
            return throwError(error);
          })
        );
    } else {
      addEditComponentObservable = of(new ComponentEntity(''));
    }

    const addEditComponentSubscription = addEditComponentObservable
      .pipe(
        mergeMap(component => {
          const navigationRequest = new ComponentOverviewNavigationRequest(this.__application!.id, this.__version!.id, component.id);
          return this._navigationService.navigateToScreenAsync(navigationRequest);
        }),
        finalize(() => this._spinerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe();
    this._subscription.add(addEditComponentSubscription);
  }

  //#endregion
}
