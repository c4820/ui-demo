import {NgModule} from '@angular/core';
import {ComponentOverviewRoutingModule} from './component-overview-routing.module';
import {ComponentOverviewComponent} from './component-overview.component';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {MarkdownModule} from 'ngx-markdown';
import {HasAnyValidatorsModule, ValidationSummarizerModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    ComponentOverviewRoutingModule,
    WidgetFabModule,
    MarkdownModule.forRoot(),
    HasAnyValidatorsModule.forRoot(),
    ValidationSummarizerModule.forRoot({}),
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    ComponentOverviewComponent
  ],
  exports: [
    ComponentOverviewComponent
  ]
})
export class ComponentOverviewModule {

}
