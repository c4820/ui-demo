import {NgModule} from '@angular/core';
import {ApplicationComponentComponent} from './application-component.component';
import {ApplicationComponentRoutingModule} from './application-component-routing.module';
import {CommonModule} from '@angular/common';
import {
  ToDetailedEventTextPipeModule
} from '../../../../pipes/to-detailed-event-text/to-detailed-event-text-pipe.module';
import {ToTrustedHtmlPipeModule} from '@ui-tool/core';
import {RouterModule} from '@angular/router';
import {
  ToDetailedMethodTextPipeModule
} from '../../../../pipes/to-detailed-method-text/to-detailed-method-text-pipe.module';
import {ApplicationComponentsPageGuard} from '../../../../guards/application-components-page.guard';
import {ApplicationComponentPageGuard} from '../../../../guards/application-component-page.guard';
import {WidgetFabModule} from '../../../shared/widgets/widget-fab';
import {ComponentOverviewPageGuard} from '../../../../guards/component-overview-page.guard';

@NgModule({
  imports: [
    ApplicationComponentRoutingModule,
    CommonModule,
    ToDetailedEventTextPipeModule,
    ToTrustedHtmlPipeModule,
    RouterModule,
    ToDetailedMethodTextPipeModule,
    WidgetFabModule
  ],
  declarations: [
    ApplicationComponentComponent
  ],
  exports: [
    ApplicationComponentComponent
  ],
  providers: [
    ComponentOverviewPageGuard,
    ApplicationComponentsPageGuard,
    ApplicationComponentPageGuard
  ]
})
export class ApplicationComponentModule {
}
