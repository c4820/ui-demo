import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComponentPropertyComponent} from './component-property.component';
import {ComponentPropertyPageGuard} from '../../../../../guards/component-property-page.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [ComponentPropertyPageGuard],
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange',
    component: ComponentPropertyComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ComponentPropertyRoutingModule {

}
