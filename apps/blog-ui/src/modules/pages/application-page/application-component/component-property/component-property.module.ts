import {NgModule} from '@angular/core';
import {ComponentPropertyComponent} from './component-property.component';
import {ComponentPropertyRoutingModule} from './component-property-routing.module';
import {ComponentPropertyPageGuard} from '../../../../../guards/component-property-page.guard';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {HasAnyValidatorsModule, ValidationSummarizerModule} from '@ui-tool/core';
import {MarkdownModule} from 'ngx-markdown';

@NgModule({
  imports: [
    ComponentPropertyRoutingModule,
    WidgetFabModule,
    CommonModule,
    ReactiveFormsModule,
    ValidationSummarizerModule.forRoot({}),
    HasAnyValidatorsModule.forRoot(),
    MarkdownModule.forRoot()
  ],
  declarations: [
    ComponentPropertyComponent
  ],
  exports: [
    ComponentPropertyComponent
  ],
  providers: [
    ComponentPropertyPageGuard
  ]
})
export class ComponentPropertyModule {

}
