import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import {Application} from '../../../../../models/entities/application';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {Component as ComponentEntity} from '../../../../../models/entities/component';
import {Property} from '../../../../../models/entities/property';
import {ComponentPropertyFields} from './component-property-fields';
import {FormGroup} from '@angular/forms';
import {ItemAvailabilities} from '../../../../../enumerations/item-availabilities';
import {Event as EventEntity} from '../../../../../models/entities/event';
import {
  ISmartNavigatorService,
  ISpinnerService,
  IValidationSummarizerService,
  SMART_NAVIGATOR_SERVICE,
  SPINNER_SERVICE,
  VALIDATION_SUMMARIZER_SERVICE
} from '@ui-tool/core';
import {ToastrService} from 'ngx-toastr';
import {finalize, forkJoin, mergeMap, Observable, of, Subscription, switchMap, tap, throwError} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {
  ComponentPropertyChosenChannelEvent
} from '../../../../../models/channel-events/component-property-chosen.channel-event';
import {Method} from '../../../../../models/entities/method';
import {HostKinds} from '../../../../../constants/host-kinds';
import {EVENT_SERVICE, METHOD_SERVICE, PROPERTY_SERVICE} from '../../../../../constants/injectors';
import {IEventService} from '../../../../../services/interfaces/apis/event-service.interface';
import {IMethodService} from '../../../../../services/interfaces/apis/method-service.interface';
import {AddPropertyViewModel} from '../../../../../view-models/properties/add-property.view-model';
import {IPropertyService} from '../../../../../services/interfaces/apis/property-service.interface';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';
import {catchError} from 'rxjs/operators';
import {
  ComponentPropertyNavigationRequest
} from '../../../../../models/navigation-requests/component-property-navigation-request';
import {ScreenCodes} from '../../../../../constants/screen-codes';
import {
  ComponentPropertiesPageQueryParams
} from '../../../../../models/query-params/component-properties-page-query-params';
import {ComponentEventsPageQueryParams} from '../../../../../models/query-params/component-events-page-query-params';
import {Router} from '@angular/router';

@Component({
  selector: 'component-property',
  templateUrl: 'component-property.component.html',
  styleUrls: ['component-property.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComponentPropertyComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region Properties

  private __application?: Application;

  private __applicationVersion?: ApplicationVersion;

  private __component?: ComponentEntity;

  private __event?: EventEntity;

  private __method?: Method;

  private __property?: Property;

  private __hostKind?: string;

  private __hostId?: string;

  // What url to go when go back button is clicked.
  private __goBackUrl?: string;

  private __fields: ComponentPropertyFields;

  protected readonly _subscription: Subscription;

  public readonly ControlNames = ComponentPropertyFields;

  public readonly Availabilities = ItemAvailabilities;

  public readonly HostKinds = HostKinds;

  //#endregion

  //#region Accessors

  public get property(): Property | undefined {
    return this.__property;
  }

  public get componentEvent(): EventEntity | undefined {
    return this.__event;
  }

  public get componentMethod(): Method | undefined {
    return this.__method;
  }

  public get hostKind(): string | undefined {
    return this.__hostKind;
  }

  public get goBackUrl(): string | undefined {
    return this.__goBackUrl;
  }

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     @Inject(EVENT_SERVICE)
                     protected readonly _eventService: IEventService,
                     @Inject(METHOD_SERVICE)
                     protected readonly _methodService: IMethodService,
                     @Inject(PROPERTY_SERVICE)
                     protected readonly _propertyService: IPropertyService,
                     @Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     @Inject(SMART_NAVIGATOR_SERVICE)
                     protected readonly _navigationService: ISmartNavigatorService,
                     protected readonly _router: Router,
                     protected readonly _toast: ToastrService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__fields = new ComponentPropertyFields();
    this._subscription = new Subscription();
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    const hookPropertyChosenSubscription = this._messageBusService
      .hookMessageChannelByType(ComponentPropertyChosenChannelEvent)
      .pipe(
        switchMap(({
                     application, version, component,
                     property, hostKind, hostId
                   }) => {

          // Display spinner.
          const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);

          let getEventObservable = of(new EventEntity(''));
          let getMethodObservable = of(new Method(''));

          if (hostKind && hostId) {
            if (hostKind === HostKinds.event) {
              getEventObservable = this._eventService.getByIdAsync(hostId);
            } else if (hostKind === HostKinds.method) {
              getMethodObservable = this._methodService.getByIdAsync(hostId);
            }
          }

          return forkJoin([of(application), of(version), of(component),
            of(property), of(hostKind), of(hostId), getEventObservable, getMethodObservable])
            .pipe(
              finalize(() => {
                this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId);
              })
            );
        })
      )
      .subscribe(([application, version, component, property,
                    hostKind, hostId, componentEvent, componentMethod]) => {
        this.__application = application;
        this.__applicationVersion = version;
        this.__component = component;
        this.__property = property;
        this.__hostKind = hostKind;
        this.__hostId = hostId;
        this.__event = componentEvent;
        this.__method = componentMethod;

        if (property && property.id && property.id.length) {
          this.formGroup.get(this.ControlNames.propertyName)?.patchValue(property.name);
          this.formGroup.get(this.ControlNames.description)?.patchValue(property.description);
          this.formGroup.get(this.ControlNames.valueType)?.patchValue(property.valueType);
          this.formGroup.get(this.ControlNames.defaultValue)?.patchValue(property.defaultValue);
          this.formGroup.get(this.ControlNames.optional)?.patchValue(property.optional);
          this.formGroup.get(this.ControlNames.availability)?.patchValue(property.availability);
        }

        switch (this.__hostKind) {
          case HostKinds.component:
          case undefined:
          case null:
            this.__goBackUrl = encodeURI(this._navigationService.buildUrlTree(ScreenCodes.componentProperties, void (0), {
              queryParams: new ComponentPropertiesPageQueryParams(this.__application!.id, this.__applicationVersion!.id,
                this.__component!.id)
            }).toString());
            break;

          case HostKinds.event:
            this.__goBackUrl = encodeURI(this._navigationService.buildUrlTree(ScreenCodes.componentEvents, void (0), {
              queryParams: new ComponentEventsPageQueryParams(this.__application!.id, this.__applicationVersion!.id, this.__component!.id)
            }).toString());
            break;

          case HostKinds.method:
            this.__goBackUrl = encodeURI(this._navigationService.buildUrlTree(ScreenCodes.componentEvents, void (0), {
              queryParams: new ComponentEventsPageQueryParams(this.__application!.id, this.__applicationVersion!.id, this.__component!.id)
            }).toString());
            break;
        }

        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookPropertyChosenSubscription);
  }

  public ngAfterViewInit(): void {
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public submit(): void {

    if (!this.formGroup) {
      return;
    }

    this._validationSummarizerService.doFormControlsValidation(this.formGroup);

    if (this.formGroup.invalid) {
      this._toast.error(`Some fields are invalid. Please check and try again.`);
      return;
    }

    const name = this.formGroup.get(this.ControlNames.propertyName)!.value;
    const description = this.formGroup.get(this.ControlNames.description)!.value;
    const valueType = this.formGroup.get(this.ControlNames.valueType)!.value;
    const defaultValue = this.formGroup.get(this.ControlNames.defaultValue)!.value;
    const optional = this.formGroup.get(this.ControlNames.optional)!.value;
    const availability = this.formGroup.get(this.ControlNames.availability)!.value;

    let addEditPropertyObservable: Observable<Property> = of(new Property(''));

    // User is editing a property.
    if (this.property && this.property.id && this.property.id.length > 0) {
      addEditPropertyObservable = of(new Property(''));
    } else {
      const command = new AddPropertyViewModel(name, description, valueType, defaultValue, optional, availability);
      if (this.__hostKind === HostKinds.method) {
        addEditPropertyObservable = this._propertyService.addToMethodAsync(this.__hostId!, command);
      } else if (this.__hostKind === HostKinds.event) {
        addEditPropertyObservable = this._propertyService.addToEventAsync(this.__hostId!, command);
      } else {
        addEditPropertyObservable = this._propertyService.addToComponentAsync(this.__hostId!, command);
      }

      addEditPropertyObservable = addEditPropertyObservable
        .pipe(
          tap(() => {
            this._toast.success(`Created property ${name} successfully,`);
          }),
          catchError(error => {
            this._toast.error(`Failed to create property ${name}`);
            return throwError(error);
          })
        );
    }

    // Display spinner
    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const addEditPropertySubscription = addEditPropertyObservable
      .pipe(
        mergeMap(property => {
          const navigationRequest = new ComponentPropertyNavigationRequest(this.__application!.id, this.__applicationVersion!.id,
            this.__component!.id, this.__hostKind!, this.__hostId!, property.id!);

          return this._navigationService.navigateToScreenAsync(navigationRequest);
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe();
    this._subscription.add(addEditPropertySubscription);
  }

  public goToUrl(url: string | undefined): void {
    if (!url) {
      return;
    }

    void this._router.navigateByUrl(url);
  }

  //#endregion

}
