import {FormControl, FormGroup, Validators} from '@angular/forms';

export class ComponentPropertyFields {

  //#region Static properties

  public static readonly propertyName = 'NAME';

  public static readonly valueType = 'VALUE_TYPE';

  public static readonly defaultValue = 'DEFAULT_VALUE';

  public static readonly description = 'DESCRIPTION';

  public static readonly optional = 'OPTIONAL';

  public static readonly availability = 'AVAILABILITY';

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [ComponentPropertyFields.propertyName]: new FormControl('', [Validators.required]),
      [ComponentPropertyFields.valueType]: new FormControl('', [Validators.required]),
      [ComponentPropertyFields.defaultValue]: new FormControl('', [Validators.required]),
      [ComponentPropertyFields.description]: new FormControl('', [Validators.required]),
      [ComponentPropertyFields.optional]: new FormControl('', [Validators.required]),
      [ComponentPropertyFields.availability]: new FormControl('', [Validators.required]),
    });
  }

  //#endregion

}
