import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {VersionOverviewComponent} from './version-overview.component';
import {ApplicationVersionPageGuard} from '../../../../../guards/application-version-page.guard';

//#region Routes

const routes: Route[] = [
  {
    path: '',
    component: VersionOverviewComponent,
    canActivate: [ApplicationVersionPageGuard],
    runGuardsAndResolvers: 'paramsOrQueryParamsChange'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

//#endregion

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class VersionOverviewRoutingModule {

}
