import {NgModule} from '@angular/core';
import {VersionOverviewComponent} from './version-overview.component';
import {VersionOverviewRoutingModule} from './version-overview-routing.module';
import {ApplicationVersionPageGuard} from '../../../../../guards/application-version-page.guard';
import {HasAnyValidatorsModule, SmartNavigatorModule, ValidationSummarizerModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';
import {WidgetFabModule} from '../../../../shared/widgets/widget-fab';
import {ReactiveFormsModule} from '@angular/forms';
import {MarkdownModule} from 'ngx-markdown';

@NgModule({
    imports: [
        VersionOverviewRoutingModule,
        SmartNavigatorModule,
        CommonModule,
        NgbAccordionModule,
        WidgetFabModule,
        ReactiveFormsModule,
        ValidationSummarizerModule.forRoot({}),
        MarkdownModule.forRoot(),
        HasAnyValidatorsModule.forRoot()
    ],
  declarations: [
    VersionOverviewComponent
  ],
  exports: [
    VersionOverviewComponent
  ],
  providers: [
    ApplicationVersionPageGuard
  ]
})
export class VersionOverviewModule {
}
