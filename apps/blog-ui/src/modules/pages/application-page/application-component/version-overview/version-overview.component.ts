import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {finalize, from, mergeMap, Observable, Subscription} from 'rxjs';
import {Application} from '../../../../../models/entities/application';
import {
  APPLICATION_SERVICE, APPLICATION_VERSION_SERVICE,
  COMPONENT_SERVICE,
  EVENT_SERVICE,
  METHOD_SERVICE,
  PROPERTY_SERVICE
} from '../../../../../constants/injectors';
import {IComponentService} from '../../../../../services/interfaces/apis/component-service.interface';
import {Component as ApplicationComponent} from '../../../../../models/entities/component';
import {ScreenCodes} from '../../../../../constants/screen-codes';
import {
  ISmartNavigatorService,
  ISpinnerService,
  IValidationSummarizerService,
  SMART_NAVIGATOR_SERVICE,
  SPINNER_SERVICE,
  VALIDATION_SUMMARIZER_SERVICE
} from '@ui-tool/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IPropertyService} from '../../../../../services/interfaces/apis/property-service.interface';
import {IMethodService} from '../../../../../services/interfaces/apis/method-service.interface';
import {IEventService} from '../../../../../services/interfaces/apis/event-service.interface';
import {IApplicationService} from '../../../../../services/interfaces/apis/application-service.interface';
import {ApplicationVersion} from '../../../../../models/entities/application-version';
import {FormGroup} from '@angular/forms';
import {VersionOverviewFields} from './version-overview-fields';
import {ToastrService} from 'ngx-toastr';
import {
  AddApplicationVersionViewModel
} from '../../../../../view-models/application-versions/add-application-version.view-model';
import {
  ApplicationVersionNavigationRequest
} from '../../../../../models/navigation-requests/application-version-navigation-request';
import {map} from 'rxjs/operators';
import {
  ApplicationOverviewNavigationRequest
} from '../../../../../models/navigation-requests/application-overview-navigation-request';
import {ComponentsNavigationRequest} from '../../../../../models/navigation-requests/components-navigation-request';
import {
  ApplicationVersionSelectedChannelEvent
} from '../../../../../models/channel-events/applications/application-version-selected.channel-event';
import {SpinnerContainerIds} from '../../../../../constants/spinner-container-ids';
import {PageViewModes} from '../../../../../enumerations/page-view-modes';
import {
  EditApplicationVersionViewModel
} from '../../../../../view-models/application-versions/edit-application-version.view-model';
import {EditableField} from '../../../../../models/editable-field';
import {
  IApplicationVersionService
} from '../../../../../services/interfaces/apis/application-version-service.interface';
import {cloneDeep} from 'lodash-es';
import {ProfileViewModel} from '../../../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../../../models/channel-events/profiles/profile-updated.channel-event';

@Component({
  selector: 'application-version-page',
  templateUrl: 'version-overview.component.html',
  styleUrls: ['version-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VersionOverviewComponent implements OnInit, OnDestroy {

  //#region Properties

  private __profile?: ProfileViewModel;

  private __viewMode: PageViewModes = PageViewModes.READ;

  private __components: ApplicationComponent[] = [];

  private __application?: Application;

  private __version?: ApplicationVersion;

  private readonly __fields = new VersionOverviewFields();

  protected _subscription = new Subscription();

  public readonly ScreenCodes = ScreenCodes;

  public readonly ControlNames = VersionOverviewFields;

  public readonly ViewModes = PageViewModes;

  //#endregion

  //#region Accessors

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  public get viewMode(): PageViewModes {
    return this.__viewMode;
  }

  public get components(): ApplicationComponent[] {
    return this.__components;
  }

  public get application(): Application | undefined {
    return this.__application;
  }

  public get version(): ApplicationVersion | undefined {
    return this.__version;
  }

  @HostBinding('class.ps-3')
  public get hasVersion(): boolean {
    return this.__version != null && this.__version.id.length > 0;
  }

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  @HostBinding('class')
  public get hostClass(): string {
    return 'd-block';
  }

  //#endregion

  //#region Constructor

  public constructor(
    @Inject(MESSAGE_BUS_SERVICE)
    protected readonly _messageBusService: IMessageBusService,
    @Inject(COMPONENT_SERVICE)
    protected readonly _componentService: IComponentService,
    @Inject(SMART_NAVIGATOR_SERVICE)
    protected readonly _navigationService: ISmartNavigatorService,
    @Inject(PROPERTY_SERVICE)
    protected readonly _propertyService: IPropertyService,
    @Inject(METHOD_SERVICE)
    protected readonly _methodService: IMethodService,
    @Inject(EVENT_SERVICE)
    protected readonly _eventService: IEventService,
    @Inject(APPLICATION_SERVICE)
    protected readonly _applicationService: IApplicationService,
    @Inject(APPLICATION_VERSION_SERVICE)
    protected readonly _applicationVersionService: IApplicationVersionService,
    @Inject(VALIDATION_SUMMARIZER_SERVICE)
    protected readonly _validationSummarizerService: IValidationSummarizerService,
    @Inject(SPINNER_SERVICE)
    protected readonly _spinnerService: ISpinnerService,
    protected readonly _toast: ToastrService,
    protected readonly _changeDetectorRef: ChangeDetectorRef,
    protected readonly _activatedRoute: ActivatedRoute,
    protected readonly _router: Router) {
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {
    this._subscription?.unsubscribe();
    this._subscription = new Subscription();

    const queryParamsChangedSubscription = this._messageBusService
      .hookMessageChannelByType(ApplicationVersionSelectedChannelEvent)
      .subscribe(({application, version}) => {
        this.__application = application;
        this.__version = version;

        if (version && version.id) {
          const titleControl = this.formGroup.get(this.ControlNames.title);
          const descriptionControl = this.formGroup.get(this.ControlNames.description);

          titleControl?.patchValue(version.title);
          descriptionControl?.patchValue(version.description);
        } else {
          this.__viewMode = PageViewModes.ADD;
        }

        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(queryParamsChangedSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public submitApplicationVersion(): void {

    if (!this.formGroup) {
      return;
    }

    const application = this.__application;
    if (!application || !application.id) {
      return;
    }

    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
    if (this.formGroup.invalid) {
      this._toast.error(`Some fields are invalid. Please check and try again.`);
      return;
    }

    const titleControl = this.formGroup.get(this.ControlNames.title)!;
    const descriptionControl = this.formGroup.get(this.ControlNames.description)!;

    let addEditVersionObservable: Observable<ApplicationVersion> | null = null;
    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);

    if (!this.version || !this.version.id) {
      const command = new AddApplicationVersionViewModel(
        application.id, titleControl.value, descriptionControl.value);

      addEditVersionObservable = this._applicationVersionService.addAsync(command);
    } else {
      const command = new EditApplicationVersionViewModel();
      command.title = new EditableField<string>(titleControl.value, titleControl.dirty || titleControl.touched);
      command.description = new EditableField<string>(descriptionControl.value, descriptionControl.dirty || descriptionControl.touched);
      addEditVersionObservable = this._applicationVersionService.editAsync(this.version.id, command);
    }

    const addApplicationVersionSubscription = addEditVersionObservable
      .pipe(
        mergeMap(applicationVersion => {
          this.__version = cloneDeep(applicationVersion);
          const navigationRequest = new ApplicationVersionNavigationRequest(application.id, applicationVersion.id);
          return this._navigationService.navigateToScreenAsync(navigationRequest)
            .pipe(
              map(() => applicationVersion)
            );
        }),
        finalize(() => {
          this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId);
        })
      )
      .subscribe(applicationVersion => {
        this._toast.success(`${applicationVersion.title} has been added successfully.`);
      });
    this._subscription.add(addApplicationVersionSubscription);
  }

  public goToUrl(url?: string): void {

    if (!url) {
      return;
    }

    const navigationSubscription = from(this._router
      .navigateByUrl(url))
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  // To go back to the previous page.
  public goBack(): void {

    if (!this.__application || !this.__application.id) {
      return;
    }

    const navigationRequest = new ApplicationOverviewNavigationRequest(this.__application?.id);
    const navigationSubscription = this._navigationService.navigateToScreenAsync(navigationRequest)
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public deleteVersion(): void {

    if (!this.__application || !this.__application.id) {
      return;
    }

    if (!this.__version || !this.__version.id) {
      return;
    }

    const displaySpinnerRequestId = this._spinnerService.displaySpinner(SpinnerContainerIds.mainLayout);
    const deleteApplicationVersionSubscription = this._applicationVersionService
      .deleteByIdAsync(this.__version.id)
      .pipe(
        mergeMap(() => {
          const navigationRequest = new ApplicationOverviewNavigationRequest(this.__application!.id);
          return this._navigationService.navigateToScreenAsync(navigationRequest);
        }),
        finalize(() => this._spinnerService.deleteSpinner(SpinnerContainerIds.mainLayout, displaySpinnerRequestId))
      )
      .subscribe();
    this._subscription.add(deleteApplicationVersionSubscription);
  }

  public toggleEdit(): void {

    // Adding new version.
    if (!this.__version || !this.__version.id) {
      return;
    }

    if (this.__viewMode === PageViewModes.READ) {
      this.__viewMode = PageViewModes.EDIT;
      return;
    }

    this.__viewMode = PageViewModes.READ;
  }

  //#endregion

}
