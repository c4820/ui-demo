import {FormControl, FormGroup, Validators} from '@angular/forms';

export class VersionOverviewFields {

  //#region Static properties

  public static readonly title = 'TITLE';

  public static readonly description = 'DESCRIPTION';

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({});
    this.__formGroup.addControl(VersionOverviewFields.title, new FormControl('', [Validators.required]));
    this.__formGroup.addControl(VersionOverviewFields.description, new FormControl('', [Validators.required]));
  }

  //#endregion

}
