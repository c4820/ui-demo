import {NgModule} from '@angular/core';
import {ApplicationPageComponent} from './application-page.component';
import {RouterModule} from '@angular/router';
import {SmartNavigatorModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {ApplicationPageRoutingModule} from './application-page-routing.module';

@NgModule({
  declarations: [
    ApplicationPageComponent
  ],
  imports: [
    RouterModule,
    ApplicationPageRoutingModule,
    SmartNavigatorModule,
    CommonModule
  ],
  exports: [
    ApplicationPageComponent
  ]
})
export class ApplicationPageModule {

}
