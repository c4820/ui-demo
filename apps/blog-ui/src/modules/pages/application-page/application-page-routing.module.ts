import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ApplicationPageComponent} from './application-page.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationPageComponent,
    children: [
      {
        path: 'overview',
        loadChildren: () => import('./application-overview/application-overview.module')
          .then(m => m.ApplicationOverviewModule)
      },
      {
        path: 'component',
        loadChildren: () => import('./application-component/application-component.module')
          .then(m => m.ApplicationComponentModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ApplicationPageRoutingModule {

}
