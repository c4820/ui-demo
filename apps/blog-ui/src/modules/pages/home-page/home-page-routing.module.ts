import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {HomePageComponent} from './home-page.component';

//#region Routes

const routes: Route[] = [
  {
    path: '',
    component: HomePageComponent
  }
];

//#endregion

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomePageRoutingModule {

}
