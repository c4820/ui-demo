import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import {ISmartNavigatorService, NavigateToScreenRequest, SMART_NAVIGATOR_SERVICE} from '@ui-tool/core';
import {ScreenCodes} from '../../../constants/screen-codes';
import {APPLICATION_SERVICE} from '../../../constants/injectors';
import {Subscription} from 'rxjs';
import {IApplicationService} from '../../../services/interfaces/apis/application-service.interface';
import {Application} from '../../../models/entities/application';
import {
  ApplicationOverviewNavigationRequest
} from '../../../models/navigation-requests/application-overview-navigation-request';
import {RequirementNames} from '../../../constants/requirement-names';
import {ProfileViewModel} from '../../../view-models/profiles/profile.view-model';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {ProfileUpdatedChannelEvent} from '../../../models/channel-events/profiles/profile-updated.channel-event';

@Component({
  selector: 'home-page',
  templateUrl: 'home-page.component.html',
  styleUrls: ['home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent implements OnInit, OnDestroy {

  //#region Properties

  private __applications: Application[];

  private __profile?: ProfileViewModel;

  protected _subscription: Subscription = new Subscription();

  public readonly ScreenCodes = ScreenCodes;

  public readonly RequirementNames = RequirementNames;

  //#endregion

  //#region Accessor

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  public get applications(): Application[] {
    return this.__applications;
  }

  @HostBinding('class')
  public get hostClass(): string {
    return 'page';
  }

  public get screenCodes(): typeof ScreenCodes {
    return ScreenCodes;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SMART_NAVIGATOR_SERVICE) protected _navigationService: ISmartNavigatorService,
                     @Inject(APPLICATION_SERVICE) protected readonly _applicationService: IApplicationService,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__applications = [];
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {
    this._subscription?.unsubscribe();
    this._subscription = new Subscription();

    const hookCategoriesSubscription = this._applicationService.getAsync()
      .subscribe(applications => {
        this.__applications = applications;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookCategoriesSubscription);

    const hookProfileSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public goToVersionsPage(applicationId: string): void {
    const navigationSubscription = this._navigationService
      .navigateToScreenAsync(new ApplicationOverviewNavigationRequest(applicationId))
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public goToAddApplicationPage(): void {
    const navigationSubscription = this._navigationService
      .navigateToScreenAsync(new ApplicationOverviewNavigationRequest())
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  //#endregion
}
