import {NgModule} from '@angular/core';
import {HomePageComponent} from './home-page.component';
import {HomePageRoutingModule} from './home-page-routing.module';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {FeatureSentinelModule, REQUIREMENT_HANDLER, SmartNavigatorModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {ApplicationModule} from './application/application.module';
import {
  AbleToAddApplicationRequirementHandler
} from '../../../services/implementations/requirement-handlers/able-to-add-application.requirement-handler';

@NgModule({
  imports: [
    RouterModule,
    HomePageRoutingModule,
    TranslateModule,
    SmartNavigatorModule,
    CommonModule,
    ApplicationModule,
    FeatureSentinelModule
  ],
  declarations: [
    HomePageComponent
  ],
  exports: [
    HomePageComponent,
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: AbleToAddApplicationRequirementHandler,
      multi: true
    }
  ]
})
export class HomePageModule {

}
