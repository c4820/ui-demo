import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'application',
  templateUrl: 'application.component.html',
  styleUrls: ['application.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationComponent {

  //#region Properties

  private __title: string;

  private __imageUrl: string;

  //#endregion

  //#region Accessors

  public get title(): string {
    return this.__title;
  }

  @Input()
  public set title(value: string) {
    this.__title = value;
  }

  public get imageUrl(): string {
    return this.__imageUrl;
  }

  @Input()
  public set imageUrl(value: string) {
    this.__imageUrl = value;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__title = '';
    this.__imageUrl = '';
  }

  //#endregion

  //#region Methods

  //#endregion
}
