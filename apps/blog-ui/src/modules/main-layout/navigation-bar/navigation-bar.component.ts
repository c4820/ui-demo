import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  NgZone,
  OnInit
} from '@angular/core';
import {ISmartNavigatorService, NavigateToScreenRequest, SMART_NAVIGATOR_SERVICE, WINDOW} from '@ui-tool/core';
import {ScreenCodes} from '../../../constants/screen-codes';
import {mergeMap, Subscription} from 'rxjs';
import {IMessageBusService, MESSAGE_BUS_SERVICE} from '@message-bus/core';
import {ChangeNavbarTitleChannelEvent} from '../../../models/channel-events/navbar/change-navbar-title.channel-event';
import {AUTHENTICATION_SERVICE} from '../../../services/authentication-service/authentication.injector';
import {IAuthenticationService} from '../../../services/authentication-service/authentication-service.interface';
import {IdpCodes} from '../../../constants/idp-codes';
import {ProfileViewModel} from '../../../view-models/profiles/profile.view-model';
import {ProfileUpdatedChannelEvent} from '../../../models/channel-events/profiles/profile-updated.channel-event';
import {HomeNavigationRequest} from '../../../models/navigation-requests/home-navigation-request';

@Component({
  selector: 'navigation-bar',
  templateUrl: 'navigation-bar.component.html',
  styleUrls: ['navigation-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationBarComponent implements OnInit, AfterViewInit {

  //#region Properties

  public readonly ScreenCodes = ScreenCodes;

  protected _subscription = new Subscription();

  private __title: string;

  private __expanded = false;

  private __profile?: ProfileViewModel;

  //#endregion

  //#region Accessors

  public get title(): string {
    return this.__title;
  }

  public get expanded(): boolean {
    return this.__expanded;
  }

  public get profile(): ProfileViewModel | undefined {
    return this.__profile;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SMART_NAVIGATOR_SERVICE)
                     protected _navigationService: ISmartNavigatorService,
                     @Inject(AUTHENTICATION_SERVICE)
                     protected readonly _authenticationService: IAuthenticationService,
                     @Inject(WINDOW) protected readonly _window: Window,
                     protected readonly _elementRef: ElementRef,
                     @Inject(MESSAGE_BUS_SERVICE)
                     protected readonly _messageBusService: IMessageBusService,
                     protected readonly _changeDetectorRef: ChangeDetectorRef) {
    this.__title = '';
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {
    this._subscription?.unsubscribe();
    this._subscription = new Subscription();

    const changeTitleSubscription = this._messageBusService
      .hookMessageChannelByType(ChangeNavbarTitleChannelEvent)
      .subscribe(({title}) => {
        this.__title = title;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(changeTitleSubscription);

    const hookProfileUpdatedSubscription = this._messageBusService
      .hookMessageChannelByType(ProfileUpdatedChannelEvent)
      .subscribe(({profile}) => {
        this.__profile = profile;
        this._changeDetectorRef.markForCheck();
      });
    this._subscription.add(hookProfileUpdatedSubscription);
  }

  public ngAfterViewInit(): void {
  }

  //#endregion

  //#region Methods

  public authenticate(): void {
    const goToIdpPageSubscription = this._authenticationService
      .goToIdpPageAsync(IdpCodes.authZero)
      .subscribe();
    this._subscription.add(goToIdpPageSubscription);
  }

  public logout(): void {
    const navigationSubscription = this._authenticationService
      .deleteAuthenticationResultAsync()
      .pipe(
        mergeMap(() => {
          this._messageBusService.addMessageInstance(new ProfileUpdatedChannelEvent(undefined));
          const navigationRequest = new HomeNavigationRequest();
          return this._navigationService.navigateToScreenAsync(navigationRequest);
        })
      )
      .subscribe();
    this._subscription.add(navigationSubscription);
  }

  public changeNavbarExpansion(): void {
    this.__expanded = !this.__expanded;
  }

  public clickGoToLanding(event: Event): void {

    if (event) {
      event.preventDefault();
    }

    this._navigationService
      .navigateToScreenAsync(new NavigateToScreenRequest(ScreenCodes.home))
      .subscribe();
  }

  //#endregion
}
