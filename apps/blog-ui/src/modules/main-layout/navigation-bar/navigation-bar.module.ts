import {NgModule} from '@angular/core';
import {NavigationBarComponent} from './navigation-bar.component';
import {SmartNavigatorModule, WindowAccessorModule} from '@ui-tool/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        WindowAccessorModule,
        RouterModule,
        SmartNavigatorModule,
        CommonModule
    ],
  declarations: [
    NavigationBarComponent
  ],
  exports: [
    NavigationBarComponent
  ]
})
export class NavigationBarModule {

}
