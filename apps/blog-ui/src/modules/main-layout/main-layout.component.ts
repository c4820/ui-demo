import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {SpinnerContainerIds} from '../../constants/spinner-container-ids';

@Component({
  selector: 'main-layout',
  templateUrl: 'main-layout.component.html',
  styleUrls: ['main-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainLayoutComponent implements OnInit {

  //#region Properties

  public readonly SpinnerContainerIds = SpinnerContainerIds;

  //#endregion

  //#region Constructor

  public constructor(@Inject(DOCUMENT) protected readonly _document: Document) {
  }

  //#endregion

  //#region Life cycle

  public ngOnInit(): void {
    this._document.body.id = 'page-top';
  }

  //#endregion

  //#region Methods

  //#endregion
}
