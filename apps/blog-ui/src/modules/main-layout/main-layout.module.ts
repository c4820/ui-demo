import {NgModule} from '@angular/core';
import {MainLayoutComponent} from './main-layout.component';
import {NavigationBarModule} from './navigation-bar/navigation-bar.module';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {SpinnerContainerModule, WindowAccessorModule} from '@ui-tool/core';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    MainLayoutComponent
  ],
  imports: [
    WindowAccessorModule,
    NavigationBarModule,
    RouterModule,
    CommonModule,
    SpinnerContainerModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      tapToDismiss: true,
      enableHtml: true,
      includeTitleDuplicates: false
    })
  ],
  exports: [
    MainLayoutComponent
  ]
})
export class MainLayoutModule {
}
