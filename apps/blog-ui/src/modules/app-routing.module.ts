import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from './main-layout/main-layout.component';
import {ApplicationPageGuard} from '../guards/application-page.guard';
import {ProfileGuard} from '../guards/profile-guard/profile.guard';

const routes: Routes = [
  {
    path: 'oauth-callback',
    loadChildren: () => import('./pages/oauth-callback/oauth-callback.module')
      .then(m => m.OauthCallbackModule)
  },
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [ProfileGuard],
    children: [
      {
        path: 'application',
        canActivate: [ApplicationPageGuard],
        loadChildren: () => import('./pages/application-page/application-page.module')
          .then(m => m.ApplicationPageModule)
      },
      {
        path: '',
        loadChildren: () => import('./pages/home-page/home-page.module').then(m => m.HomePageModule)
      },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    onSameUrlNavigation: 'reload',
    enableTracing: false
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
