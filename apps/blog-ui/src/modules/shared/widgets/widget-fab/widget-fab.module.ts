import {NgModule} from '@angular/core';
import {WidgetFabComponent} from './widget-fab.component';
import {NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {WidgetFabItemDirective} from './widget-fab-item/widget-fab-item.directive';

@NgModule({
  imports: [
    CommonModule,
    NgbDropdownModule,
    NgbTooltipModule
  ],
  declarations: [
    WidgetFabItemDirective,
    WidgetFabComponent
  ],
  exports: [
    WidgetFabItemDirective,
    WidgetFabComponent
  ]
})
export class WidgetFabModule {

}
