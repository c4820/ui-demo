export class WidgetFabItemContext {

  //#region Properties

  public readonly children: WidgetFabItemContext[];

  //#endregion

  //#region Constructor

  public constructor(public readonly id: string, public readonly hostId: string) {
    this.children = [];
  }

  //#endregion

  //#region Accessors

  public get hasChildren(): boolean {
    return this.children && this.children.length > 0;
  }

  //#endregion
}
