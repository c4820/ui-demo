import {AfterContentInit, Directive, EventEmitter, Inject, Input, Optional, Output, SkipSelf} from '@angular/core';
import {WidgetFabComponent} from '../widget-fab.component';
import {v4 as uuid} from 'uuid';

@Directive({
  selector: 'widget-fab widget-fab-item, widget-fab-item widget-fab-item'
})
export class WidgetFabItemDirective implements AfterContentInit {

  //#region Properties

  private _hostId: string;

  private _id: string;

  @Input()
  public icon?: string;

  // tslint:disable-next-line:no-input-rename
  @Input('theme-color')
  public themeColor?: string;

  @Input()
  public title?: string;

  @Input()
  public disabled = false;

  //#endregion

  //#region Accessors

  public get id(): string {
    return this._id;
  }

  public get hostId(): string {
    return this._hostId;
  }

  //#endregion

  //#region Events

  // tslint:disable-next-line:no-output-rename no-output-native
  @Output('click')
  public clickEvent: EventEmitter<void> = new EventEmitter<void>();

  //#endregion

  //#region Constructor

  public constructor(@Inject(WidgetFabComponent) @Optional() widgetFabContainer: WidgetFabComponent,
                     @Inject(WidgetFabItemDirective) @SkipSelf() @Optional() widgetFabItem: WidgetFabItemDirective) {

    this._hostId = widgetFabItem?.id || widgetFabContainer.id;
    this._id = uuid();

  }

  //#endregion

  //#region Life cycle hooks

  public ngAfterContentInit(): void {
  }

  //#endregion

  //#region Methods

  //#endregion
}
