import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  Input,
  OnDestroy,
  OnInit,
  QueryList
} from '@angular/core';
import {WidgetFabItemDirective} from './widget-fab-item/widget-fab-item.directive';
import {Subscription} from 'rxjs';
import {v4 as uuid} from 'uuid';
import {WIDGET_FAB_THEME_COLOR} from './widget-fab-constants';
import {WidgetFabItemContext} from './widget-fab-item/widget-fab-item-context';

@Component({
  selector: 'widget-fab',
  templateUrl: './widget-fab.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WidgetFabComponent implements OnInit, AfterContentInit, OnDestroy {

  //#region Properties

  // Menu context.
  private _context?: WidgetFabItemContext;

  // Id to directive
  private _idToDirective: Record<string, WidgetFabItemDirective>;

  // Host id.
  private readonly _hostId = uuid();

  // Id
  private readonly _id = uuid();

  // Subscription watcher.
  protected readonly _subscription: Subscription;

  @Input('theme-color')
  public themeColor?: WIDGET_FAB_THEME_COLOR;

  @Input()
  public icon?: string;

  @Input()
  public title?: string;

  // Read the items inside the widget fab
  @ContentChildren(WidgetFabItemDirective, {descendants: true})
  public childItems!: QueryList<WidgetFabItemDirective>;

  //#endregion

  //#region Accessors

  public get hostId(): string {
    return this._hostId;
  }

  public get id(): string {
    return this._id;
  }

  public get context(): WidgetFabItemContext | undefined {
    return this._context;
  }

  public get idToDirective(): { [id: string]: WidgetFabItemDirective } {
    return this._idToDirective;
  }

  //#endregion

  //#region Constructor

  public constructor(protected readonly changeDetectorRef: ChangeDetectorRef) {
    this._idToDirective = {};
    this._subscription = new Subscription();
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {
  }

  public ngAfterContentInit(): void {

    // Do context reload.
    this.doContextReload();

    const hookChildItemsChangesSubscription = this.childItems.changes
      .subscribe(() => {
        this.doContextReload();
      });
    this._subscription.add(hookChildItemsChangesSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }

  //#endregion

  //#region Methods

  public clickOnItem(item: WidgetFabItemDirective): void {
    item.clickEvent.emit();
  }

  //#endregion

  //#region Internal methods

  protected constructWidgetItemTree(context: WidgetFabItemContext,
                                    childItems: WidgetFabItemDirective[]): void {

    for (const childItem of childItems) {

      if (childItem.hostId !== context.id) {
        continue;
      }

      const childContext = new WidgetFabItemContext(childItem.id, context.id);
      this.constructWidgetItemTree(childContext, childItems);
      context.children.push(childContext);
    }
  }

  // Do item reload.
  protected doContextReload(): void {
    this._context = undefined;
    this._idToDirective = {};

    // Id to directive
    const idToDirective: Record<string, WidgetFabItemDirective> = {};

    const childItems = this.childItems.toArray();
    for (const directive of childItems) {
      idToDirective[directive.id] = directive;
    }
    this._idToDirective = idToDirective;

    const context = new WidgetFabItemContext(this._id, this.hostId);
    this.constructWidgetItemTree(context, childItems);
    this._context = context;
    this.changeDetectorRef.markForCheck();
  }

  //#endregion

}
