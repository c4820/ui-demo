export declare type WIDGET_FAB_THEME_COLOR = 'primary' | 'secondary' | 'danger' | 'warning' | 'info' | 'success' |
  'outline-primary' | 'outline-secondary' | 'outline-danger' | 'outline-warning' | 'outline-info' | 'outline-success' | string;
