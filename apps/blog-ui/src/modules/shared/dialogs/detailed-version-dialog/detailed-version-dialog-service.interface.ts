import {Observable} from 'rxjs';
import {
  AddApplicationVersionViewModel
} from '../../../../view-models/application-versions/add-application-version.view-model';

export interface IDetailedVersionDialogService {

  //#region Methods

  displayAsync(): Observable<AddApplicationVersionViewModel>;

  //#endregion

}
