import {Injectable} from '@angular/core';
import {IDetailedVersionDialogService} from './detailed-version-dialog-service.interface';
import {from, Observable} from 'rxjs';
import {
  AddApplicationVersionViewModel
} from '../../../../view-models/application-versions/add-application-version.view-model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DetailedVersionDialogComponent} from './detailed-version-dialog.component';

@Injectable()
export class DetailedVersionDialogService implements IDetailedVersionDialogService {

  //#region Constructor

  public constructor(protected readonly _ngbModal: NgbModal) {
  }

  //#endregion

  //#region Methods

  public displayAsync(): Observable<AddApplicationVersionViewModel> {
    const dialogRef = this._ngbModal.open(DetailedVersionDialogComponent, {
      size: 'lg',
      backdrop: 'static'
    });

    return from(dialogRef.result);
  }

  //#endregion

}
