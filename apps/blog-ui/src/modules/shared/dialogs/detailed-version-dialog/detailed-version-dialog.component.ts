import {ChangeDetectionStrategy, Component} from '@angular/core';
import {DetailedVersionDialogFields} from './detailed-version-dialog-fields';
import {FormGroup} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'detailed-version-dialog',
  templateUrl: 'detailed-version-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailedVersionDialogComponent {

  //#region Properties

  private readonly __fields = new DetailedVersionDialogFields();

  private __title = '';

  public readonly ControlNames = DetailedVersionDialogFields;

  //#endregion

  //#region Accessors

  public get title(): string {
    return this.__title;
  }

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(protected readonly _activeModal: NgbActiveModal) {
  }

  //#endregion

  //#region Methods

  public confirm(): void {

  }

  public dismiss(): void {
    this._activeModal.dismiss('MANUALLY_CLOSE');
  }

  //#endregion
}
