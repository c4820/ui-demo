import {InjectionToken} from '@angular/core';
import {IDetailedVersionDialogService} from './detailed-version-dialog-service.interface';

export const DETAILED_VERSION_DIALOG_SERVICE = new InjectionToken<IDetailedVersionDialogService>('DETAILED_VERSION_DIALOG_SERVICE');
