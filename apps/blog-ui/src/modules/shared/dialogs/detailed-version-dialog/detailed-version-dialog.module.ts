import {NgModule} from '@angular/core';
import {DetailedVersionDialogComponent} from './detailed-version-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ValidationSummarizerModule} from '@ui-tool/core';
import {DETAILED_VERSION_DIALOG_SERVICE} from './injector';
import {DetailedVersionDialogService} from './detailed-version-dialog.service';

@NgModule({
  declarations: [
    DetailedVersionDialogComponent
  ],
  imports: [
    ReactiveFormsModule,
    ValidationSummarizerModule
  ],
  exports: [
    DetailedVersionDialogComponent
  ],
  providers: [
    {
      provide: DETAILED_VERSION_DIALOG_SERVICE,
      useClass: DetailedVersionDialogService
    }
  ]
})
export class DetailedVersionDialogModule {
}
