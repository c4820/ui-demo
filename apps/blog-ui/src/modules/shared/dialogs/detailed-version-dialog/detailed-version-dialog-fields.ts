import {FormControl, FormGroup, Validators} from '@angular/forms';

export class DetailedVersionDialogFields {

  //#region Static properties

  public static readonly title = 'title';

  public static readonly description = 'description';

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [DetailedVersionDialogFields.title]: new FormControl('', [Validators.required]),
      [DetailedVersionDialogFields.description]: new FormControl('', [Validators.required])
    });
  }

  //#endregion

}
