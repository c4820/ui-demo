export class ApplicationMessageEvents {

  //#region Properties

  public static readonly applicationSelected = 'APPLICATION_SELECTED';

  public static readonly applicationVersionSelected = 'APPLICATION_VERSION_SELECTED';

  //#endregion

}
