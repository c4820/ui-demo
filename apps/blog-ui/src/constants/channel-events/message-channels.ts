export class MessageChannels {

  //#region Properties

  public static readonly application = 'APPLICATION';

  public static readonly navbar = 'NAVBAR';

  public static readonly component = 'COMPONENT';

  public static readonly profile = 'profile';

  //#endregion

}
