export class CodeExampleFilePathConstant {
  public static readonly importVsmModule = '/assets/code-example/basic-validator/import-module.txt';
  public static readonly customValidatorConstant = '/assets/code-example/basic-validator/custom-validator-constant.txt';
  public static readonly cmsValidatorSelector = '/assets/code-example/basic-validator/cms-validator-selector.txt';
  public static readonly vmsApiProperties = '/assets/code-example/basic-validator/vms-api-propertes-table.txt';
}
