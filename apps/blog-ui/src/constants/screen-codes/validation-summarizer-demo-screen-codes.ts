export class ValidationSummarizerDemoScreenCodes {

  //#region Properties

  public static readonly withApi = 'VALIDATION_SUMMARIZER_DEMO-WITH_API';

  public static readonly withModuleDescription = 'VALIDATION_SUMMARIZER_DEMO-WITH_MODULE_DESCRIPTION';

  public static readonly withMessageTemplate = 'VALIDATION_SUMMARIZER_DEMO-WITH_MESSAGE_TEMPLATE';

  public static readonly withVisibilityHandler = 'VALIDATION_SUMMARIZER_DEMO-WITH_VISIBILITY_HANDLER';

  public static readonly withTemplateDriven = 'VALIDATION_SUMMARIZER_DEMO-WITH_TEMPLATE_DRIVEN';

  public static readonly withCustomValidator = 'VALIDATION_SUMMARIZER_DEMO-WITH_CUSTOM_VALIDATOR';

  public static readonly withBasicValidator = 'VALIDATION_SUMMARIZER_DEMO-WITH_BASIC_VALIDATOR';

  public static readonly withChildValidatorModule = 'VALIDATION_SUMMARIZER_DEMO-WITH_CHILD_VALIDATOR_MODULE';

  public static readonly withValidationItemTemplate = 'VALIDATION_SUMMARIZER_DEMO-WITH_VALIDATION_ITEM_TEMPLATE';

  //#endregion

}
