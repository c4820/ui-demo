export class HostKinds {

  //#region Properties

  public static readonly method = 'method';

  public static readonly event = 'event';

  public static readonly component = 'component';

  //#endregion

}
