import {InjectionToken, Type} from '@angular/core';
import {EndpointResolver} from '../services/implementations/endpoint.resolver';
import {IAppSettingsService} from '../services/interfaces/app-settings-service.interface';
import {IApplicationService} from '../services/interfaces/apis/application-service.interface';
import {IComponentService} from '../services/interfaces/apis/component-service.interface';
import {IPropertyService} from '../services/interfaces/apis/property-service.interface';
import {IMethodService} from '../services/interfaces/apis/method-service.interface';
import {IEventService} from '../services/interfaces/apis/event-service.interface';

// Application service.
export const APP_SETTINGS_SERVICE = new InjectionToken<IAppSettingsService>('APP_SETTINGS_SERVICE');
export const ENDPOINT_RESOLVER = new InjectionToken<EndpointResolver>('ENDPOINT_RESOLVER');

export const APPLICATION_SERVICE = new InjectionToken<IApplicationService>('APPLICATION_SERVICE');
export const APPLICATION_VERSION_SERVICE = new InjectionToken<IApplicationService>('APPLICATION_VERSION_SERVICE');
export const COMPONENT_SERVICE = new InjectionToken<IComponentService>('COMPONENT_SERVICE');
export const PROPERTY_SERVICE = new InjectionToken<IPropertyService>('PROPERTY_SERVICE');
export const METHOD_SERVICE = new InjectionToken<IMethodService>('METHOD_SERVICE');
export const EVENT_SERVICE = new InjectionToken<IEventService>('EVENT_SERVICE');
