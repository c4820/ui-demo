export class ScreenCodes {

  //#region Properties

  public static readonly home = 'HOME';

  public static readonly applicationOverview = 'APPLICATION_OVERVIEW';

  public static readonly applicationVersion = 'APPLICATION_VERSION';

  public static readonly application = 'DETAILED_APPLICATION';

  public static readonly applicationComponents = 'APPLICATION_COMPONENTS';

  public static readonly componentOverview = 'COMPONENT_OVERVIEW';

  public static readonly componentEvents = 'COMPONENT_EVENTS';

  public static readonly componentEvent = 'COMPONENT_EVENT';

  public static readonly componentMethods = 'COMPONENT_METHODS';

  public static readonly componentMethod = 'COMPONENT_METHOD';

  public static readonly componentProperties = 'COMPONENT_PROPERTIES';

  public static readonly componentProperty = 'COMPONENT_PROPERTY';

  //#endregion

}
