export class ComponentKinds {

  //#region Properties

  public static readonly service = 'service';

  public static readonly component = 'component';

  //#endregion

}
